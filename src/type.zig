const std = @import("std");
// types
pub const string = []const u8;
pub fn ConstSlice(comptime T: type) type {
    return []const T;
}

pub fn Tuple2(comptime T1: type, comptime T2: type) type {
    return struct { T1, T2 };
}

fn string_eql(s1: []const u8, s2: []const u8) bool {
    return std.mem.eql(u8, s1, s2);
}

// type inferers
pub fn TypeOfReturn(comptime F: type) type {
    return switch (@typeInfo(F)) {
        .Pointer => |p| TypeOfReturn(p.child),
        .Fn => |f| f.return_type.?,
        else => @compileError(@typeName(F)),
    };
}

pub fn TypeOfField(comptime T: type, comptime fname: []const u8) type {
    switch (@typeInfo(T)) {
        .Struct => |s| blk: {
            inline for (s.fileds) |f| {
                if (string_eql(f.name, fname)) {
                    break :blk f;
                }
            }
        },
        .Union => |u| blk: {
            inline for (u.fileds) |f| {
                if (string_eql(f.name, fname)) {
                    break :blk f;
                }
            }
        },
        .Enum => |e| blk: {
            inline for (e.fileds) |f| {
                if (string_eql(f.name, fname)) {
                    break :blk f;
                }
            }
        },
    }

    @compileError("has no filed \"" ++ fname ++ "\"!");
}
