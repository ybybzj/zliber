fn Result(comptime T: type, comptime E: type) type {
    return E!T;
}

pub fn Promise(comptime T: type) type {
    return struct {
        value: ?T,

        cb: ?*anyopaque = null,
    };
}
