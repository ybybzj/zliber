const std = @import("std");
const Atomic = std.atomic.Value;
const AtomicOrder = std.builtin.AtomicOrder;

pub usingnamespace std.atomic;

pub fn tryCompareAndSwap(comptime T: type, atomic: *Atomic(T), expected_value: T, new_value: T, comptime success_order: AtomicOrder, comptime fail_order: AtomicOrder) ?T {
    return atomic.cmpxchgWeak(expected_value, new_value, success_order, fail_order);
}

pub fn compareAndSwap(comptime T: type, atomic: *Atomic(T), expected_value: T, new_value: T, comptime success_order: AtomicOrder, comptime fail_order: AtomicOrder) ?T {
    return atomic.cmpxchgStrong(expected_value, new_value, success_order, fail_order);
}
