const std = @import("std");
const assert = std.debug.assert;
pub const Level = std.log.Level;
const Scope = @Type(.EnumLiteral);
pub const scoped = std.log.scoped;
pub const err = std.log.err;
pub const warn = std.log.warn;
pub const info = std.log.info;
pub const debug = std.log.debug;

pub const ScopeLevel = struct { scope: []const u8, level: Level };

const Config = struct { log_level: Level = std.log.default_level, log_scope_levels: []const ScopeLevel = &.{} };

var config = Config{};

pub fn set_log_level(comptime level: Level) void {
    config.log_level = level;
}

pub fn set_scopes(comptime scopes: anytype) void {
    const n: comptime_int = scopes.len;
    comptime var scope_levels = [_]ScopeLevel{.{ .level = .info, .scope = undefined }} ** n;
    const fields = @typeInfo(@TypeOf(scopes)).Struct.fields;
    inline for (fields, 0..) |field, i| {
        scope_levels[i].scope = @field(scopes, field.name);
    }
    config.log_scope_levels = &scope_levels;
}

pub fn set_scope_levels(comptime scope_levels: []const ScopeLevel) void {
    config.log_scope_levels = scope_levels;
}

pub fn logFn(comptime level: Level, comptime scope: Scope, comptime format: []const u8, args: anytype) void {
    // std.debug.print("using mylogFn...\n", .{});
    if (!logEnable(level, scope)) return;
    defaultLog(level, scope, format, args);
}

fn logEnable(comptime level: Level, comptime scope: Scope) bool {
    for (config.log_scope_levels) |level_scope| {
        const scopeName = @tagName(scope);
        if (std.mem.eql(u8, level_scope.scope, scopeName)) return @intFromEnum(level) <= @intFromEnum(level_scope.level);
    }
    if (scope == std.log.default_log_scope) {
        return @intFromEnum(level) <= @intFromEnum(config.log_level);
    }
    return false;
}

fn defaultLog(
    comptime message_level: Level,
    comptime scope: @Type(.EnumLiteral),
    comptime format: []const u8,
    args: anytype,
) void {
    const level_txt = comptime message_level.asText();
    const prefix2 = if (scope == .default) ": " else "(" ++ @tagName(scope) ++ "): ";
    const stderr = std.io.getStdErr().writer();
    std.debug.getStderrMutex().lock();
    defer std.debug.getStderrMutex().unlock();
    nosuspend stderr.print("[" ++ level_txt ++ "]" ++ prefix2 ++ format ++ "\n", args) catch return;
}

pub fn print(s: []const u8) void {
    std.debug.print("{s}", .{s});
}

pub fn printLn(s: []const u8) void {
    std.debug.print("{s}\n", .{s});
}

pub fn fprint(comptime fmt: []const u8, args: anytype) void {
    std.debug.print(fmt, args);
}

pub fn fprintLn(comptime fmt: []const u8, args: anytype) void {
    std.debug.print(fmt ++ "\n", args);
}
