const std = @import("std");
pub usingnamespace std.mem;

pub inline fn getFixedBufferAllocator(comptime len: usize) std.mem.Allocator {
    var buf: [len]u8 = [_]u8{undefined} ** len;
    var fba = std.heap.FixedBufferAllocator.init(&buf);
    return fba.allocator();
}

pub fn clone(comptime T: type, allocator: *const std.mem.Allocator, src: []const T) ![]const T {
    const buf = try allocator.alloc(T, src.len);
    @memcpy(buf, src);
    return buf;
}

pub fn include(comptime T: type, a: []const T, b: T) bool {
    if (a.len == 0) return false;
    for (a) |a_elem| {
        if (a_elem == b) return true;
    }
    return false;
}
