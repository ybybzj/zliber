const std = @import("std");
const Allocator = std.mem.Allocator;
const builtin = @import("builtin");
const os = std.os;
const assert = std.debug.assert;

const net = std.net;
pub const Address = net.Address;

pub const ServAddressHints = struct {
    family: enum { ip4, ip6, unspec } = .unspec,
    protocal: enum { tcp, udp } = .tcp,
};

fn hintsinfoFromHins(hints: ServAddressHints) if (builtin.target.os.tag == .windows) os.windows.ws2_32.addrinfo else os.addrinfo {
    const family: i32 = switch (hints.family) {
        .ip4 => os.AF.INET,
        .ip6 => os.AF.INET6,
        .unspec => os.AF.UNSPEC,
    };

    const protocol: i32 = switch (hints.protocal) {
        .tcp => os.IPPROTO.TCP,
        .udp => os.IPPROTO.UDP,
    };

    if (builtin.target.os.tag == .windows) {
        return os.windows.ws2_32.addrinfo{
            .flags = os.windows.ws2_32.AI.PASSIVE,
            .family = family,
            .socktype = os.SOCK.STREAM,
            .protocol = protocol,
            .canonname = null,
            .addr = null,
            .addrlen = 0,
            .next = null,
        };
    }

    if (builtin.link_libc) {
        const sys = if (builtin.target.os.tag == .windows) os.windows.ws2_32 else os.system;

        return os.addrinfo{ .flags = sys.AI.PASSIVE, .family = family, .socktype = os.SOCK.STREAM, .protocol = protocol, .canonname = null, .addr = null, .addrlen = 0, .next = null };
    }

    @compileError("net.hintsinfoFromHins unimplemented for this OS");
}

const GetServAddressParams = struct { name: ?[]const u8 = null, port: u16, hints: ServAddressHints = .{} };
pub fn getServAddress(allocator: Allocator, params: GetServAddressParams) !Address {
    var name_c: ?[:0]u8 = null;
    if (params.name) |name| {
        name_c = try allocator.dupeZ(u8, name);
        defer allocator.free(name_c.?);
    }

    const name_ptr = if (name_c) |name| name.ptr else null;

    const port_c = try std.fmt.allocPrintZ(allocator, "{}", .{params.port});
    defer allocator.free(port_c);

    const hints = hintsinfoFromHins(params.hints);

    const freeaddrinfo = if (builtin.target.os.tag == .windows) os.windows.ws2_32.freeaddrinfo else os.system.freeaddrinfo;

    var res: ?*os.addrinfo = null;
    if (builtin.target.os.tag == .windows) {
        const ws2_32 = os.windows.ws2_32;

        var first = true;
        while (true) {
            const rc = ws2_32.getaddrinfo(name_ptr, port_c.ptr, &hints, &res);
            switch (@as(os.windows.ws2_32.WinsockError, @enumFromInt(@as(u16, @intCast(rc))))) {
                @as(os.windows.ws2_32.WinsockError, @enumFromInt(0)) => break,
                .WSATRY_AGAIN => return error.TemporaryNameServerFailure,
                .WSANO_RECOVERY => return error.NameServerFailure,
                .WSAEAFNOSUPPORT => return error.AddressFamilyNotSupported,
                .WSA_NOT_ENOUGH_MEMORY => return error.OutOfMemory,
                .WSAHOST_NOT_FOUND => return error.UnknownHostName,
                .WSATYPE_NOT_FOUND => return error.ServiceUnavailable,
                .WSAEINVAL => unreachable,
                .WSAESOCKTNOSUPPORT => unreachable,
                .WSANOTINITIALISED => {
                    if (!first) return error.Unexpected;
                    first = false;
                    try os.windows.callWSAStartup();
                    continue;
                },
                else => |err| return os.windows.unexpectedWSAError(err),
            }
        }
    }

    if (builtin.link_libc) {
        const sys = if (builtin.target.os.tag == .windows) os.windows.ws2_32 else os.system;
        switch (sys.getaddrinfo(name_ptr, port_c.ptr, &hints, &res)) {
            @as(sys.EAI, @enumFromInt(0)) => {},
            .ADDRFAMILY => return error.HostLacksNetworkAddresses,
            .AGAIN => return error.TemporaryNameServerFailure,
            .BADFLAGS => unreachable, // Invalid hints
            .FAIL => return error.NameServerFailure,
            .FAMILY => return error.AddressFamilyNotSupported,
            .MEMORY => return error.OutOfMemory,
            .NODATA => return error.HostLacksNetworkAddresses,
            .NONAME => return error.UnknownHostName,
            .SERVICE => return error.ServiceUnavailable,
            .SOCKTYPE => unreachable, // Invalid socket type requested in hints
            .SYSTEM => switch (os.errno(-1)) {
                else => |e| return os.unexpectedErrno(e),
            },
            else => unreachable,
        }
    }
    defer if (res) |some| freeaddrinfo(some);

    assert(res != null);
    assert(res.?.addr != null);

    return Address.initPosix(@alignCast(res.?.addr.?));
}
