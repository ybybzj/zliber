/// refer: https://github.com/tigerbeetle/tigerbeetle/blob/d1805daf33ebec8cec8728b476f69d9ec1f0a741/src/time.zig
const std = @import("std");
const builtin = @import("builtin");

const os = std.os;
const assert = std.debug.assert;
const is_darwin = builtin.target.os.tag.isDarwin();
const is_windows = builtin.target.os.tag == .windows;

const is_posix = switch (builtin.os.tag) {
    .wasi => builtin.link_libc,
    .windows => false,
    else => true,
};

pub const Time = struct {
    const Self = @This();

    /// Hardware and/or software bugs can mean that the monotonic clock may regress.
    /// One example (of many): https://bugzilla.redhat.com/show_bug.cgi?id=448449
    /// We crash the process for safety if this ever happens, to protect against infinite loops.
    /// It's better to crash and come back with a valid monotonic clock than get stuck forever.
    monotonic_guard: u64 = 0,
    /// A timestamp to measure elapsed time, meaningful only on the same system, not across reboots.
    /// Always use a monotonic timestamp if the goal is to measure elapsed time.
    /// This clock is not affected by discontinuous jumps in the system time, for example if the
    /// system administrator manually changes the clock.
    /// return nano seconds
    pub fn monotonic(self: *Self) u64 {
        const m = blk: {
            // QPC on windows doesn't fail on >= XP/2000 and includes time suspended.
            if (builtin.os.tag == .windows) {
                break :blk os.windows.QueryPerformanceCounter();
            }

            // On WASI without libc, use clock_time_get directly.
            if (builtin.os.tag == .wasi and !builtin.link_libc) {
                var ns: os.wasi.timestamp_t = undefined;
                const rc = os.wasi.clock_time_get(os.wasi.CLOCK.MONOTONIC, 1, &ns);
                if (rc != .SUCCESS) @panic("monotonic clock is unsupported!");
                break :blk ns;
            }

            // On darwin, use UPTIME_RAW instead of MONOTONIC as it ticks while suspended.
            // On linux, use BOOTTIME instead of MONOTONIC as it ticks while suspended.
            // On freebsd derivatives, use MONOTONIC_FAST as currently there's no precision tradeoff.
            // On other posix systems, MONOTONIC is generally the fastest and ticks while suspended.
            const clock_id = switch (builtin.os.tag) {
                .macos, .ios, .tvos, .watchos => os.CLOCK.UPTIME_RAW,
                .freebsd, .dragonfly => os.CLOCK.MONOTONIC_FAST,
                .linux => os.CLOCK.BOOTTIME,
                else => os.CLOCK.MONOTONIC,
            };

            var ts: os.timespec = undefined;
            os.clock_gettime(clock_id, &ts) catch @panic("monotonic clock is unsupported!");
            break :blk @as(u64, @intCast(ts.tv_sec)) * std.time.ns_per_s + @as(u64, @intCast(ts.tv_nsec));
        };
        // "Oops!...I Did It Again"
        if (m < self.monotonic_guard) @panic("a hardware/kernel bug regressed the monotonic clock");
        self.monotonic_guard = m;
        return m;
    }
};
