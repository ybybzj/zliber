const std = @import("std");
const time = std.time;
const epoch = time.epoch;

pub const Date = struct {
    year: u16,
    month: std.time.epoch.Month,
    day: u5, // 0 to 30
    hour: u5, // 0 to 23
    minute: u6, // 0 to 59
    second: u6, // 0 to 59
    const Self = @This();
    pub fn now() Date {
        const _timestamp_now = time.timestamp();
        if (_timestamp_now < 0) @panic("invalid current time!");
        return fromEpochSeconds(@as(u64, @intCast(_timestamp_now)));
    }

    pub fn fromEpochSeconds(secs: u64) Date {
        const now_epoch = epoch.EpochSeconds{ .secs = secs };
        const epoch_day = now_epoch.getEpochDay();
        const year_day = epoch_day.calculateYearDay();
        const month_day = year_day.calculateMonthDay();
        const day_secs = now_epoch.getDaySeconds();
        return Date{ .year = year_day.year, .month = month_day.month, .day = month_day.day_index, .hour = day_secs.getHoursIntoDay(), .minute = day_secs.getMinutesIntoHour(), .second = day_secs.getSecondsIntoMinute() };
    }

    pub fn toEpochSeconds(self: Self) u64 {
        const secs_in_day: u64 = @as(u64, self.hour) * 3600 + @as(u64, self.minute) * 60 + self.second;

        var days_in_years: u64 = blk: {
            var days_of_months: u17 = self.day;
            var m = @intFromEnum(self.month) - 1;
            const leap_kind: epoch.YearLeapKind = if (epoch.isLeapYear(self.year)) .leap else .not_leap;
            while (m > 0) : (m -= 1) {
                const days_of_month = epoch.getDaysInMonth(leap_kind, @as(epoch.Month, @enumFromInt(m)));
                days_of_months += days_of_month;
            }
            break :blk days_of_months;
        };

        var year = self.year - 1;
        while (year >= epoch.epoch_year) : (year -= 1) {
            const year_size = epoch.getDaysInYear(year);
            days_in_years += year_size;
        }

        const secs_in_years = days_in_years * epoch.secs_per_day;

        return secs_in_years + secs_in_day;
    }

    pub fn format(self: Self, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = options;
        _ = fmt;
        try std.fmt.format(writer, "{d}/{d}/{d} {d:0>2}:{d:0>2}:{d:0>2}", .{ self.year, self.month.numeric(), self.day, self.hour, self.minute, self.second });
    }
};
