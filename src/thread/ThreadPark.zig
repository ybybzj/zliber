const std = @import("std");
const Atomic = std.atomic.Value;
const Futex = std.Thread.Futex;

atom: Atomic(u32) = Atomic(u32).init(0),

const Self = @This();
pub fn wait(self: *Self) void {
    Futex.wait(&self.atom, 0);
}

pub fn wait_timeout(self: *Self, timeout_ms: u64) void {
    return Futex.timedWait(&self.atom, 0, timeout_ms * std.time.ns_per_ms) catch {};
}

pub fn wake(self: *Self, max_waiters: u32) void {
    Futex.wake(&self.atom, max_waiters);
}

pub fn wakeOne(self: *Self) void {
    self.wake(1);
}
