const std = @import("std");
const ThreadPool = @import("./ThreadPool.zig");
const Allocator = std.mem.Allocator;
const IntrusiveMaker = @import("../ds/intrusive.zig").Maker;

pub const Task = struct { runner: *const fn (*anyopaque) void, context: *anyopaque };

tp: ThreadPool,
completions: Queue = .{},
allocator: Allocator,

const TaskQueue = @This();
const TP_Task = struct {
    next: ?*TP_Task = null,
    task: ThreadPool.Task = .{ .callback = cb },
    parent: *TaskQueue,

    context: *anyopaque,
    run: *const fn (*anyopaque) void,

    fn cb(t: *ThreadPool.Task, tp: *ThreadPool) void {
        _ = tp;
        var self = @fieldParentPtr(TP_Task, "task", t);
        self.run(self.context);
        self.parent.completions.enqueue(self);
    }

    fn init(parent: *TaskQueue, runner: *const fn (*anyopaque) void, context: *anyopaque) TP_Task {
        return .{ .parent = parent, .context = context, .run = runner };
    }
};

const List = IntrusiveMaker(TP_Task).FIFO;
const Queue = IntrusiveMaker(TP_Task).Queue;

pub fn init(thread_num: u32, allocator: Allocator) TaskQueue {
    return .{ .tp = ThreadPool.init(.{ .max_threads = thread_num }), .allocator = allocator };
}

pub fn deinit(self: *TaskQueue) void {
    self.tp.shutdown();
    self.tp.deinit();
    _ = self.consumeCompletions(null);
}

pub fn isIdle(self: TaskQueue) bool {
    return self.tp.isIdle();
}

pub fn shutdown(self: *TaskQueue) void {
    self.tp.shutdown();
}

pub fn runTask(self: *TaskQueue, task: Task) !void {
    const tp_task = try self.allocator.create(TP_Task);
    tp_task.* = TP_Task.init(self, task.runner, task.context);

    var list = List{};
    list.push(tp_task);
    self.schedule(&list);
}

pub fn runTasks(self: *TaskQueue, tasks: []const Task) !void {
    var list = List{};
    errdefer {
        while (list.pop()) |taskp| {
            self.allocator.destroy(taskp);
        }
    }
    for (tasks) |task| {
        const tp_taskp = try self.allocator.create(TP_Task);

        tp_taskp.* = TP_Task.init(self, task.runner, task.context);
        list.push(tp_taskp);
    }

    self.schedule(&list);
}

inline fn schedule(self: *TaskQueue, tasks: *List) void {
    var batch = ThreadPool.Batch{};
    while (tasks.pop()) |task| {
        batch.add(&task.task);
    }

    self.tp.schedule(batch);
}

pub fn consumeCompletions(self: *TaskQueue, completionContexts: ?[]*anyopaque) usize {
    var consummer = self.completions.tryAcquireConsumer() catch return 0;
    defer self.completions.releaseConsumer(consummer);

    var completion_count: usize = 0;

    if (completionContexts) |_completionContexts| {
        for (_completionContexts) |*context| {
            const task = self.completions.pop(&consummer);
            if (task) |_task| {
                completion_count += 1;
                context.* = _task.context;
                self.allocator.destroy(_task);
            } else {
                break;
            }
        }
    } else {
        while (self.completions.pop(&consummer)) |task| {
            self.allocator.destroy(task);
            completion_count += 1;
        }
    }

    return completion_count;
}
