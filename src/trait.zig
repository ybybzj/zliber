const std = @import("std");
const meta = std.meta;

pub const TraitFn = fn (comptime type) bool;
const mem = std.mem;

fn isContainer(comptime T: type) bool {
    return switch (@typeInfo(T)) {
        .Struct => true,
        .Union => true,
        .Enum => true,
        else => false,
    };
}

pub fn isFieldOf(comptime name: []const u8, comptime id: std.builtin.TypeId) TraitFn {
    return struct {
        pub fn trait(comptime T: type) bool {
            const fields = switch (@typeInfo(T)) {
                .Struct => |s| s.fields,
                .Union => |u| u.fields,
                .Enum => |e| e.fields,
                .ErrorSet => |errors| errors.?,
                else => return false,
            };

            inline for (fields) |field| {
                if (mem.eql(u8, field.name, name)) {
                    return id == @typeInfo(field.type);
                }
            }

            return false;
        }
    }.trait;
}

pub fn isDeclOf(comptime name: []const u8, comptime id: std.builtin.TypeId) TraitFn {
    return struct {
        pub fn trait(comptime T: type) bool {
            if (!isContainer(T)) return false;
            if (!@hasDecl(T, name)) return false;
            const DT = @TypeOf(@field(T, name));
            return @typeInfo(DT) == id;
        }
    }.trait;
}

pub const TraitItem = struct { enum(u1) { field, decl }, []const u8, *const TraitFn };

inline fn getFieldType(comptime T: type, comptime name: []const u8) ?type {
    const fields = switch (@typeInfo(T)) {
        .Struct => |s| s.fields,
        .Union => |u| u.fields,
        .Enum => |e| e.fields,
        .ErrorSet => |errors| errors.?,
        else => return null,
    };

    inline for (fields) |field| {
        if (mem.eql(u8, field.name, name)) {
            return field.type;
        }
    }

    return null;
}
pub fn ContainerTrait(comptime traitItems: []const TraitItem) TraitFn {
    return struct {
        pub fn trait(comptime T: type) bool {
            if (!isContainer(T)) return false;

            inline for (traitItems) |traitItem| {
                const kind = traitItem[0];
                const name = traitItem[1];
                const traitFn = traitItem[2];
                const item_type: ?type = switch (kind) {
                    .field => getFieldType(T, name),
                    .decl => if (@hasDecl(T, name)) @TypeOf(@field(T, name)) else null,
                };
                if (item_type) |itype| {
                    if (!traitFn(itype)) {
                        @compileLog(@tagName(kind) ++ " of '" ++ name ++ "' is not qualified!");
                        return false;
                    }
                } else {
                    @compileLog(@tagName(kind) ++ " of '" ++ name ++ "' is missing!");
                    return false;
                }
            }
            return true;
        }
    }.trait;
}

fn isType(comptime T: type) TraitFn {
    return struct {
        pub fn trait(comptime _T: type) bool {
            return _T == T;
        }
    }.trait;
}

inline fn isStruct(comptime T: type) bool {
    return switch (@typeInfo(T)) {
        .Struct => true,
        else => false,
    };
}

fn isTypeInfo(comptime id: std.builtin.TypeId) TraitFn {
    const Closure = struct {
        pub fn trait(comptime T: type) bool {
            return id == @typeInfo(T);
        }
    };
    return Closure.trait;
}

pub fn TraitFromStruct(comptime S: type) TraitFn {
    std.debug.assert(isStruct(S));

    const fields = meta.fields(S);

    const declarations = meta.declarations(S);

    var trait_tiems: [fields.len + declarations.len]TraitItem = undefined;
    var i = 0;
    inline for (fields, 0..) |field, _i| {
        const field_item = .{ .field, field.name, isType(field.type) };
        trait_tiems[_i] = field_item;
        i = _i;
    }

    inline for (declarations, i + 1..) |decl, _i| {
        const decl_type = meta.activeTag(@typeInfo(@TypeOf(@field(S, decl.name))));
        const decl_item = .{ .decl, decl.name, isTypeInfo(decl_type) };
        trait_tiems[_i] = decl_item;
    }

    return ContainerTrait(&trait_tiems);
}

pub fn implTrait(comptime traitFn: TraitFn, comptime T: type) type {
    std.debug.assert(traitFn(T));
    return T;
}
