const std = @import("std");
const FIFOMaker = @import("./fifo.zig").FIFO;
const assert = std.debug.assert;
const atomic = @import("../atomic.zig");
const Atomic = atomic.Value;

const trait = @import("../trait.zig");
/// Linked list intrusive memory node and data structures to operate with it
pub fn Maker(comptime Node: type) type {
    assert(trait.TraitFromStruct(struct { next: ?*Node })(Node));
    return struct {
        /// simple first-in/first-out queue, not thread-safe
        pub const FIFO = FIFOMaker(Node);

        /// A linked list of Nodes
        pub const List = struct {
            head: *Node,
            tail: *Node,
        };

        /// An unbounded lock-free multi-producer-(non blocking)-multi-consumer queue of Node pointers.
        pub const Queue = struct {
            stack: Atomic(usize) = Atomic(usize).init(0),
            cache: ?*Node = null,

            const HAS_CACHE: usize = 0b01;
            const IS_CONSUMING: usize = 0b10;
            const PTR_MASK: usize = ~(HAS_CACHE | IS_CONSUMING);

            comptime {
                assert(@alignOf(Node) >= ((IS_CONSUMING | HAS_CACHE) + 1));
            }

            pub fn enqueue(noalias self: *Queue, node: *Node) void {
                assert(node.next == null);
                self.push(.{
                    .head = node,
                    .tail = node,
                });
            }

            pub fn push(noalias self: *Queue, list: List) void {
                var stack = self.stack.load(.monotonic);
                while (true) {
                    // Attach the list to the stack (pt. 1)
                    list.tail.next = @as(?*Node, @ptrFromInt(stack & PTR_MASK));

                    // Update the stack with the list (pt. 2).
                    // Don't change the HAS_CACHE and IS_CONSUMING bits of the consumer.
                    var new_stack = @intFromPtr(list.head);
                    assert(new_stack & ~PTR_MASK == 0);
                    new_stack |= (stack & ~PTR_MASK);

                    // Push to the stack with a release barrier for the consumer to see the proper list links.
                    stack = atomic.tryCompareAndSwap(
                        usize,
                        &self.stack,
                        stack,
                        new_stack,
                        .release,
                        .monotonic,
                    ) orelse break;
                }
            }

            pub fn tryAcquireConsumer(self: *Queue) error{ Empty, Contended }!?*Node {
                var stack = self.stack.load(.monotonic);
                while (true) {
                    if (stack & IS_CONSUMING != 0)
                        return error.Contended; // The queue already has a consumer.
                    if (stack & (HAS_CACHE | PTR_MASK) == 0)
                        return error.Empty; // The queue is empty when there's nothing cached and nothing in the stack.

                    // When we acquire the consumer, also consume the pushed stack if the cache is empty.
                    var new_stack = stack | HAS_CACHE | IS_CONSUMING;
                    if (stack & HAS_CACHE == 0) {
                        assert(stack & PTR_MASK != 0);
                        new_stack &= ~PTR_MASK;
                    }

                    // Acquire barrier on getting the consumer to see cache/Node updates done by previous consumers
                    // and to ensure our cache/Node updates in pop() happen after that of previous consumers.
                    stack = atomic.tryCompareAndSwap(
                        usize,
                        &self.stack,
                        stack,
                        new_stack,
                        .acquire,
                        .monotonic,
                    ) orelse return self.cache orelse @as(*Node, @ptrFromInt(stack & PTR_MASK));
                }
            }

            pub fn releaseConsumer(noalias self: *Queue, noalias consumer: ?*Node) void {
                // Stop consuming and remove the HAS_CACHE bit as well if the consumer's cache is empty.
                // When HAS_CACHE bit is zeroed, the next consumer will acquire the pushed stack nodes.
                var remove = IS_CONSUMING;
                if (consumer == null)
                    remove |= HAS_CACHE;

                // Release the consumer with a release barrier to ensure cache/node accesses
                // happen before the consumer was released and before the next consumer starts using the cache.
                self.cache = consumer;
                const stack = self.stack.fetchSub(remove, .release);
                assert(stack & remove != 0);
            }

            pub fn pop(noalias self: *Queue, noalias consumer_ref: *?*Node) ?*Node {
                // Check the consumer cache (fast path)
                if (consumer_ref.*) |node| {
                    consumer_ref.* = node.next;
                    return node;
                }

                // Load the stack to see if there was anything pushed that we could grab.
                var stack = self.stack.load(.monotonic);
                assert(stack & IS_CONSUMING != 0);
                if (stack & PTR_MASK == 0) {
                    return null;
                }

                // Nodes have been pushed to the stack, grab then with an Acquire barrier to see the Node links.
                stack = self.stack.swap(HAS_CACHE | IS_CONSUMING, .acquire);
                assert(stack & IS_CONSUMING != 0);
                assert(stack & PTR_MASK != 0);

                const node = @as(*Node, @ptrFromInt(stack & PTR_MASK));
                consumer_ref.* = node.next;
                return node;
            }
        };
    };
}
