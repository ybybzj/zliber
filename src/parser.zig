const std = @import("std");
const t = @import("./type.zig");
const log = @import("log.zig").scoped(.parser);
const assert = std.debug.assert;
const mem = @import("mem.zig");
const Allocator = mem.Allocator;

const Pair = t.Tuple2;

pub const Error = error{
    ParseError,
} || Allocator.Error;

pub fn MakeParser(comptime T: type) type {
    const Slice = t.ConstSlice(T);

    return struct {
        fn ResultData(comptime R: type, comptime Ctx: type) type {
            return struct { value: R, slice: Slice = &.{}, ctx: Ctx };
        }

        fn ParseResult(comptime R: type, comptime Ctx: type) type {
            return Error!?ResultData(R, Ctx);
        }

        fn EvalFn(comptime R: type, comptime Ctx: type) type {
            return *const fn (Allocator, Slice) ParseResult(R, Ctx);
        }

        pub const Parser = union(enum) {
            Literal: Literal,
            Map: Map,
            Is: Is,
            Return: Return,
            Fail: Fail,

            Product: Product,
            Either: Either,
            Many: Many,
            Left: Left,
            Right: Right,
            Fold: Fold,

            Rec: Rec,

            pub fn Result(comptime self: Parser) type {
                return switch (self) {
                    inline else => |p| p.Result(),
                };
            }

            pub fn Context(comptime self: Parser) type {
                return switch (self) {
                    inline else => |parser| blk: {
                        const P = @TypeOf(parser);
                        const Ctx = if (@hasDecl(P, "Context")) P.Context(parser) else void;
                        break :blk Ctx;
                    },
                };
            }

            pub fn parse(comptime self: Parser, allocator: Allocator, input: Slice) ParseResult(self.Result(), self.Context()) {
                return switch (self) {
                    inline else => |p| p.parse(allocator, input),
                };
            }

            pub fn freeResult(comptime self: Parser, r: ?ResultData(self.Result(), self.Context()), allcator: Allocator) void {
                if (r) |res| {
                    switch (self) {
                        inline else => |p| {
                            const P = @TypeOf(p);
                            if (@hasDecl(P, "freeResult")) {
                                p.freeResult(res, allcator);
                            }
                        },
                    }
                }
            }

            // combinators
            pub fn mapAlloc(comptime self: Parser, comptime MR: type, comptime mapFn: MapFn(self.Result(), MR), comptime freeFn: ?FreeFn(MR)) Parser {
                return .{ .Map = .{ .parser = &self, .mapType = MR, .mapFn = mapFn, .freeFn = freeFn } };
            }

            pub fn map(comptime self: Parser, comptime MR: type, comptime mapFn: *const fn (self.Result()) ?MR) Parser {
                const R = self.Result();
                return .{ .Map = .{ .parser = &self, .mapFn = struct {
                    fn f(r: R, allocator: Allocator) !?MR {
                        _ = allocator;
                        return mapFn(r);
                    }
                }.f, .mapType = MR } };
            }

            pub fn product(comptime self: Parser, comptime p: Parser) Parser {
                return .{ .Product = .{ .left_p = &self, .right_p = &p } };
            }

            pub fn either(comptime self: Parser, comptime p: Parser) Parser {
                return .{ .Either = .{ .left_p = &self, .right_p = &p } };
            }

            pub fn many(comptime self: Parser) Parser {
                return .{ .Many = .{
                    .parser = &self,
                } };
            }

            pub fn manyOne(comptime self: Parser) Parser {
                return .{ .Many = .{ .parser = &self, .allowEmpty = false } };
            }

            pub fn left(comptime self: Parser, comptime p: Parser) Parser {
                return .{ .Left = .{ .left_p = &self, .right_p = &p } };
            }

            pub fn right(comptime self: Parser, comptime p: Parser) Parser {
                return .{ .Right = .{ .left_p = &self, .right_p = &p } };
            }

            pub fn fold(comptime self: Parser, comptime init_p: Parser, comptime foldFn: FoldFn(init_p.Result(), self.Result())) Parser {
                return .{ .Fold = .{ .parser = &self, .init_p = &init_p, .foldFn = foldFn } };
            }
        };

        // Atomic Parser Types
        const Literal = struct {
            literal: Slice,
            const Self = @This();

            pub fn Result(comptime self: Self) type {
                _ = self;
                return Slice;
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), void) {
                _ = allocator;
                const len = self.literal.len;
                if (input.len < len) return null;
                if (mem.eql(T, self.literal, input[0..len])) {
                    return .{ .value = self.literal, .slice = input[len..], .ctx = {} };
                } else {
                    return null;
                }
            }
        };

        const Is = struct {
            pred: *const fn (T) bool,
            const Self = @This();
            pub fn Result(comptime self: Self) type {
                _ = self;
                return T;
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), void) {
                _ = allocator;
                if (input.len <= 0) {
                    return null;
                }
                if (self.pred(input[0])) {
                    return .{ .value = input[0], .slice = input[1..], .ctx = {} };
                } else {
                    return null;
                }
            }
        };

        const Return = struct {
            retType: type,
            retFn: *const anyopaque,
            freeFn: ?*const anyopaque = null,
            const Self = @This();
            pub fn Result(comptime self: Self) type {
                return self.retType;
            }
            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), void) {
                const retFn = castPtr(RetFn(self.retType), self.retFn);
                const value = try retFn(allocator);
                return .{ .value = value, .slice = input, .ctx = {} };
            }

            pub fn freeResult(comptime self: Self, r: ResultData(self.Result(), void), allocator: Allocator) void {
                if (self.freeFn) |freeFn_ptr| {
                    const freeFn = castPtr(FreeFn(self.retType), freeFn_ptr);
                    freeFn(r.value, allocator);
                }
            }
        };

        const Fail = struct {
            resultType: type,
            msg: []const u8 = "",
            const Self = @This();
            pub fn Result(comptime self: Self) type {
                return self.resultType;
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), void) {
                _ = input;
                _ = allocator;
                return null;
            }
        };

        // to be improved, use with precaution
        const Rec = struct {
            resultType: type,
            recFn: *const fn (Parser) Parser,
            const Self = @This();
            pub fn Result(comptime self: Self) type {
                return self.resultType;
            }
            pub fn Context(comptime self: Self) type {
                _ = self;
                return *const anyopaque;
            }

            fn parser(comptime self: Self) Parser {
                return self.recFn(.{ .Rec = self });
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), self.Context()) {
                const p: Parser = self.parser();
                assert(p.Result() == self.Result());
                const res = try p.parse(allocator, input);
                if (res) |res_r| {
                    const ctx = try allocator.create(p.Context());
                    errdefer {
                        p.freeResult(res, allocator);
                    }
                    ctx.* = res_r.ctx;
                    return .{ .value = res_r.value, .slice = res_r.slice, .ctx = ctx };
                } else {
                    return null;
                }
            }

            pub fn freeResult(comptime self: Self, r: ResultData(self.Result(), self.Context()), allocator: Allocator) void {
                const p: Parser = self.parser();
                const ctx = castPtr(*const p.Context(), r.ctx);
                defer {
                    allocator.destroy(ctx);
                }
                p.freeResult(.{ .value = r.value, .ctx = ctx.* }, allocator);
            }
        };

        // Combinator Parser Types
        const Map = struct {
            mapType: type,
            mapFn: *const anyopaque,
            freeFn: ?*const anyopaque = null,
            parser: *const Parser,

            const Self = @This();

            pub fn Result(comptime self: Self) type {
                return self.mapType;
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), void) {
                const base_p = self.parser;
                const PR = base_p.Result();
                const MR = self.mapType;
                const mapFn = castPtr(MapFn(PR, MR), self.mapFn);
                const res = try base_p.parse(allocator, input);
                defer {
                    base_p.freeResult(res, allocator);
                }
                if (res) |res_r| {
                    const value = try mapFn(res_r.value, allocator);
                    return if (value) |v| .{ .value = v, .slice = res_r.slice, .ctx = {} } else null;
                } else {
                    return null;
                }
            }

            pub fn freeResult(comptime self: Self, r: ResultData(self.Result(), void), allocator: Allocator) void {
                if (self.freeFn) |freeFn_ptr| {
                    const freeFn = castPtr(FreeFn(self.Result()), freeFn_ptr);
                    freeFn(r.value, allocator);
                }
            }
        };

        const Product = struct {
            left_p: *const Parser,
            right_p: *const Parser,
            const Self = @This();

            pub fn Result(comptime self: Self) type {
                return Pair(self.left_p.Result(), self.right_p.Result());
            }

            pub fn Context(comptime self: Self) type {
                const left_p = self.left_p;
                const right_p = self.right_p;
                return Pair(left_p.Context(), right_p.Context());
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), self.Context()) {
                const left_p = self.left_p;
                const right_p = self.right_p;
                const lres = try left_p.parse(allocator, input);
                if (lres) |lres_r| {
                    const slice = lres_r.slice;
                    const lvalue = lres_r.value;
                    const lctx = lres_r.ctx;
                    const rres = try right_p.parse(allocator, slice);
                    errdefer {
                        left_p.freeResult(lres, allocator);
                    }
                    if (rres) |rres_r| {
                        return .{ .value = .{ lvalue, rres_r.value }, .slice = rres_r.slice, .ctx = .{ lctx, rres_r.ctx } };
                    } else {
                        left_p.freeResult(lres, allocator);
                        return null;
                    }
                } else {
                    return null;
                }
            }

            pub fn freeResult(comptime self: Self, r: ResultData(self.Result(), self.Context()), allocator: Allocator) void {
                const left_p = self.left_p;
                const right_p = self.right_p;

                const lres = .{ .value = r.value[0], .ctx = r.ctx[0] };
                const rres = .{ .value = r.value[1], .ctx = r.ctx[1] };

                left_p.freeResult(lres, allocator);
                right_p.freeResult(rres, allocator);
            }
        };

        const Either = struct {
            left_p: *const Parser,
            right_p: *const Parser,
            const Self = @This();
            pub fn Result(comptime self: Self) type {
                const LR = self.left_p.Result();
                const RR = self.right_p.Result();
                assert(LR == RR);
                return LR;
            }

            pub fn Context(comptime self: Self) type {
                const LC = self.left_p.Context();
                const RC = self.right_p.Context();
                return union(enum) { left: LC, right: RC };
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), self.Context()) {
                const lres = try self.left_p.parse(allocator, input);
                if (lres) |lres_r| {
                    return .{ .value = lres_r.value, .slice = lres_r.slice, .ctx = .{ .left = lres_r.ctx } };
                } else {
                    const rres = try self.right_p.parse(allocator, input);
                    if (rres) |rres_r| {
                        return .{ .value = rres_r.value, .slice = rres_r.slice, .ctx = .{ .right = rres_r.ctx } };
                    } else {
                        return null;
                    }
                }
            }

            pub fn freeResult(comptime self: Self, r: ResultData(self.Result(), self.Context()), allocator: Allocator) void {
                switch (r.ctx) {
                    .left => |ctx| self.left_p.freeResult(.{ .value = r.value, .ctx = ctx }, allocator),
                    .right => |ctx| self.right_p.freeResult(.{ .value = r.value, .ctx = ctx }, allocator),
                }
            }
        };

        const Many = struct {
            parser: *const Parser,
            allowEmpty: bool = true,
            const Self = @This();
            pub fn Result(comptime self: Self) type {
                const R = self.parser.Result();
                return []const R;
            }

            pub fn Context(comptime self: Self) type {
                const C = self.parser.Context();
                return if (C == void) void else []C;
            }

            fn needAlloc(comptime self: Self) bool {
                return switch (self.parser.*) {
                    .Is => false,
                    else => true,
                };
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), self.Context()) {
                const R = self.parser.Result();
                const PCtx = self.parser.Context();

                if (comptime self.needAlloc() == false) {
                    var slice = input;
                    var i: usize = 0;
                    (blk: {
                        while (self.parser.parse(allocator, slice) catch |e| break :blk e) |self_r| {
                            i += 1;
                            slice = self_r.slice;
                        }
                    }) catch |e| {
                        log.warn("[Parser.many] error \"{s}\" happend!", .{@errorName(e)});
                    };
                    if (self.allowEmpty or i > 0) {
                        return .{ .value = input[0..i], .slice = slice, .ctx = {} };
                    } else {
                        return null;
                    }
                } else {
                    var arrList = try std.ArrayList(R).initCapacity(allocator, 10);
                    defer {
                        arrList.deinit();
                    }

                    var ctxArrList = if (PCtx != void) std.ArrayList(PCtx).init(allocator) else {};
                    defer {
                        if (PCtx != void) {
                            ctxArrList.deinit();
                        }
                    }

                    var slice = input;
                    var p_res: ?ResultData(R, PCtx) = null;
                    (blk: {
                        while (self.parser.parse(allocator, slice) catch |e| break :blk e) |self_r| {
                            p_res = self_r;
                            arrList.append(self_r.value) catch break :blk error.NeedFree;
                            if (PCtx != void) {
                                ctxArrList.append(self_r.ctx) catch {
                                    _ = arrList.swapRemove(arrList.items.len - 1);
                                    break :blk error.NeedFree;
                                };
                            }
                            slice = self_r.slice;
                        }
                    }) catch |e| {
                        if (e == error.NeedFree and p_res != null) {
                            self.parser.freeResult(p_res.?, allocator);
                            p_res = null;
                        }
                        log.warn("[Parser.many] error \"{s}\" happend!", .{@errorName(e)});
                    };

                    const v = try arrList.toOwnedSlice();
                    errdefer {
                        for (arrList.items, 0..) |*_item, i| {
                            const ctxi = if (PCtx != void) ctxArrList.items[i] else {};
                            self.parser.freeResult(.{ .value = _item.*, .ctx = ctxi }, allocator);
                        }
                    }

                    const ctx = if (PCtx != void) try ctxArrList.toOwnedSlice() else {};
                    errdefer {
                        if (PCtx != void) {
                            for (v, 0..) |*value, i| {
                                const ctxi = ctxArrList.items[i];
                                self.parser.freeResult(.{ .value = value.*, .ctx = ctxi }, allocator);
                            }
                        }
                    }
                    if (self.allowEmpty or v.len > 0) {
                        return .{ .value = v, .slice = slice, .ctx = ctx };
                    } else {
                        allocator.free(v);
                        if (PCtx != void) {
                            allocator.free(ctx);
                        }
                        return null;
                    }
                }
            }

            pub fn freeResult(comptime self: Self, r: ResultData(self.Result(), self.Context()), allocator: Allocator) void {
                if (comptime self.needAlloc()) {
                    const Ctx = self.Context();
                    for (r.value, 0..) |*value, i| {
                        const ctx = if (Ctx != void) r.ctx[i] else {};
                        self.parser.freeResult(.{ .value = value.*, .ctx = ctx }, allocator);
                    }
                    allocator.free(r.value);

                    if (Ctx != void) {
                        allocator.free(r.ctx);
                    }
                }
            }
        };

        const Left = struct {
            left_p: *const Parser,
            right_p: *const Parser,
            const Self = @This();

            pub fn Result(comptime self: Self) type {
                return self.left_p.Result();
            }

            pub fn Context(comptime self: Self) type {
                return self.left_p.Context();
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), self.Context()) {
                const lres = try self.left_p.parse(allocator, input);
                if (lres) |lres_r| {
                    const rres = try self.right_p.parse(allocator, lres_r.slice);
                    defer {
                        self.right_p.freeResult(rres, allocator);
                    }
                    errdefer {
                        self.left_p.freeResult(lres, allocator);
                    }
                    if (rres) |rres_r| {
                        return .{ .value = lres_r.value, .slice = rres_r.slice, .ctx = lres_r.ctx };
                    } else {
                        self.left_p.freeResult(lres, allocator);
                        return null;
                    }
                } else {
                    return null;
                }
            }

            pub fn freeResult(comptime self: Self, r: ResultData(self.Result(), self.Context()), allocator: Allocator) void {
                self.left_p.freeResult(r, allocator);
            }
        };

        const Right = struct {
            left_p: *const Parser,
            right_p: *const Parser,
            const Self = @This();

            pub fn Result(comptime self: Self) type {
                return self.right_p.Result();
            }

            pub fn Context(comptime self: Self) type {
                return self.right_p.Context();
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), self.Context()) {
                const lres = try self.left_p.parse(allocator, input);
                defer {
                    self.left_p.freeResult(lres, allocator);
                }

                if (lres) |lres_r| {
                    const rres = try self.right_p.parse(allocator, lres_r.slice);
                    if (rres) |rres_r| {
                        return .{ .value = rres_r.value, .slice = rres_r.slice, .ctx = rres_r.ctx };
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }

            pub fn freeResult(comptime self: Self, r: ResultData(self.Result(), self.Context()), allocator: Allocator) void {
                self.right_p.freeResult(r, allocator);
            }
        };

        const Fold = struct {
            parser: *const Parser,
            init_p: *const Parser,
            foldFn: *const anyopaque,
            const Self = @This();
            pub fn Result(comptime self: Self) type {
                return self.init_p.Result();
            }

            pub fn Context(comptime self: Self) type {
                return self.init_p.Context();
            }

            pub fn parse(comptime self: Self, allocator: Allocator, input: Slice) ParseResult(self.Result(), self.Context()) {
                const self_p = self.parser;
                const init_p = self.init_p;

                const foldFn = castPtr(FoldFn(init_p.Result(), self_p.Result()), self.foldFn);
                // don't free the init result here, because the value is supposed to be returned
                const init = try init_p.parse(allocator, input);

                if (init) |init_r| {
                    var res = init_r.value;
                    var slice = init_r.slice;
                    (blk: {
                        while (self_p.parse(allocator, slice) catch |e| break :blk e) |s_r| {
                            res = foldFn(res, s_r.value) catch |err| break :blk err;
                            defer {
                                self_p.freeResult(s_r, allocator);
                            }
                            slice = s_r.slice;
                        }
                    }) catch |err| {
                        log.warn("[Parser.fold] error \"{s}\" happend!", .{@errorName(err)});
                    };
                    return .{ .value = res, .slice = slice, .ctx = init_r.ctx };
                } else {
                    return null;
                }
            }

            pub fn freeResult(comptime self: Self, r: ResultData(self.Result(), self.Context()), allocator: Allocator) void {
                self.init_p.freeResult(.{ .value = r.value, .ctx = r.ctx }, allocator);
            }
        };
        // constructors
        pub fn literal(comptime s: Slice) Parser {
            return .{ .Literal = .{ .literal = s } };
        }

        pub fn is(comptime pred: *const fn (T) bool) Parser {
            return .{ .Is = .{ .pred = pred } };
        }

        pub fn oneOf(comptime slice: Slice) Parser {
            return .{ .Is = .{ .pred = struct {
                fn f(c: T) bool {
                    return mem.include(T, slice, c);
                }
            }.f } };
        }

        pub fn item(comptime c: T) Parser {
            return .{ .Is = .{ .pred = struct {
                fn f(_c: T) bool {
                    return c == _c;
                }
            }.f } };
        }

        pub fn ret(comptime R: type, comptime retFn: RetFn(R), comptime freeFn: ?FreeFn(R)) Parser {
            return .{ .Return = .{ .retType = R, .retFn = retFn, .freeFn = freeFn } };
        }

        pub fn retConst(comptime R: type, comptime r: R) Parser {
            return ret(R, struct {
                fn f(allocator: Allocator) !R {
                    _ = allocator;
                    return r;
                }
            }.f, null);
        }

        pub fn fail(comptime R: type) Parser {
            return .{ .Fail = .{ .resultType = R } };
        }

        pub fn choose(comptime parsers: []const Parser) Parser {
            assert(parsers.len > 0);
            var p = parsers[0];
            inline for (parsers[1..]) |_p| {
                p = p.either(_p);
            }
            return p;
        }

        pub fn rec(comptime R: type, comptime recFn: *const fn (Parser) Parser) Parser {
            return .{ .Rec = .{
                .resultType = R,
                .recFn = recFn,
            } };
        }
    };
}

// type helpers
fn RetFn(comptime R: type) type {
    return *const fn (Allocator) Allocator.Error!R;
}

fn MapFn(comptime T: type, comptime R: type) type {
    return *const fn (T, Allocator) Allocator.Error!?R;
}

fn FreeFn(comptime T: type) type {
    return *const fn (T, Allocator) void;
}

fn FoldFn(comptime R: type, comptime T: type) type {
    return *const fn (R, T) Error!R;
}

inline fn castPtr(comptime T: type, ptr: *const anyopaque) T {
    return @as(T, @ptrCast(@alignCast(ptr)));
}
