pub usingnamespace if (@import("root") == @This()) struct {
    pub fn main() !void {}
} else struct {};

pub fn PromiseOf(comptime T: type, comptime E: type) type {
    const State = union(enum) {
        pending,
        fulfill: T,
        fail: E,

        const Self = @This();
        pub fn changeState(self: *Self, state: Self) bool {
            switch (self.*) {
                .pending => {
                    if (state != .pending) {
                        self.* = state;
                        return true;
                    } else {
                        return false;
                    }
                },
                inline else => false,
            }
        }
    };
    return struct {
        state: State,
        onFulfilled: ?*anyopaque = null,
        onReject: ?*anyopaque = null,

        // private
        _thened: bool = false,

        const Self = @This();

        pub fn resolved(v: T) Self {
            return .{
                .state = .{ .fulfill = v },
            };
        }

        pub fn failed(e: E) Self {
            return .{
                .state = .{ .fail = e },
            };
        }

        pub fn then(self: Self, comptime R: type, comptime onFulfilled: ?*const fn (T) R, comptime onReject: ?*const fn (T) R) Self {
            if (self._thened == true) {
                return self;
            }

            self._thened = true;

            switch (self.state) {
                .pending => {
                    self.onFulfilled = onFulfilled;
                    self.onReject = onReject;
                    return self;
                },
            }
        }
    };
}
