const std = @import("std");
const assert = std.debug.assert;
const testing = std.testing;
const Thread = std.Thread;

const Atomic = std.atomic.Value;
var num_done: Atomic(usize) = Atomic(usize).init(0);

fn println(comptime fmt: []const u8, args: anytype) void {
    std.debug.print(fmt ++ "\n", args);
}

pub fn main() !void {
    println("Is multi_threaded: {}", .{!@import("builtin").single_threaded});
    try example_tp();
}

fn sleep(ms: u64) void {
    std.time.sleep(ms * std.time.ns_per_ms);
}

// wait and wake ----------------------------------------------
const ThreadPark = @import("zliber").thread.ThreadPark;

fn example_1() !void {
    var thread_park = ThreadPark{};
    (try Thread.spawn(.{}, struct {
        pub fn f(thread_park_p: *ThreadPark) void {
            for (0..10) |_| {
                sleep(50); // do some work
                _ = num_done.fetchAdd(1, .monotonic);
                thread_park_p.wakeOne();
            }
        }
    }.f, .{&thread_park})).detach();

    while (true) {
        const n = num_done.load(.unordered);
        if (n == 10) break;
        println("Working.. {}/10 done", .{n});
        thread_park.wait_timeout(1000);
    }

    println("Done!", .{});
}

// ----------------------------------------------------
fn example_2() !void {
    const Static = struct {
        var X: Atomic(i32) = Atomic(i32).init(0);
        var Y: Atomic(i32) = Atomic(i32).init(0);
    };

    const t2 = (try Thread.spawn(.{}, struct {
        pub fn f() void {
            const y = Static.Y.load(.unordered);
            const x = Static.X.load(.unordered);

            println("{} {}", .{ x, y });
        }
    }.f, .{}));
    const t1 = (try Thread.spawn(.{}, struct {
        pub fn f() void {
            Static.X.store(10, .unordered);
            Static.Y.store(20, .unordered);
        }
    }.f, .{}));

    t2.join();

    t1.join();
}

// -----------------------------------------------------
fn example_3() !void {
    const S = struct {
        var Data: u64 = 0;
        var READY: Atomic(bool) = Atomic(bool).init(false);
    };
    (try Thread.spawn(.{}, struct {
        pub fn f() void {
            println("[thread]Data is {}", .{S.Data});
            sleep(2000);
            S.READY.store(true, .unordered);
            S.Data = 123;
        }
    }.f, .{})).detach();

    S.Data = 12;

    while (!S.READY.load(.unordered)) {
        sleep(100);
        println("waiting...", .{});
    }
    println("Data = {}", .{S.Data});
}

// ------------------------------------------------------
// Spin lock
// ------------------------------------------------------

const SpinLock = struct {
    locked: Atomic(bool) = Atomic(bool).init(false),
    const Self = @This();

    pub fn lock(self: *Self) void {
        while (self.locked.swap(true, .acquire)) {
            std.atomic.spinLoopHint();
        }
    }

    pub fn unlock(self: *Self) void {
        self.locked.store(false, .release);
    }
};

fn example_4() !void {
    const S = struct {
        var val: i32 = 0;
        var spin_lock = SpinLock{};
    };

    const thread_num = 10;
    var threads: [thread_num]Thread = undefined;

    for (&threads) |*thread| {
        thread.* = try Thread.spawn(.{}, struct {
            fn f() void {
                S.spin_lock.lock();
                defer S.spin_lock.unlock();
                for (0..10000) |_| {
                    S.val += 1;
                }
            }
        }.f, .{});
    }

    for (threads) |thread| {
        thread.join();
    }

    println("val = {}", .{S.val});
}

// -----------------------------------
// Semaphore
// -----------------------------------
fn t_print(comptime fmt: []const u8, args: anytype) void {
    const pid = Thread.getCurrentId();
    std.debug.print("({})", .{pid});
    println(" " ++ fmt, args);
}
const Semaphore = Thread.Semaphore;

fn example_5() !void {
    const TestContext = struct {
        sem: *Semaphore,
        n: *i32,
        fn worker(ctx: *@This()) void {
            t_print("before wait: n = {}", .{ctx.n.*});

            ctx.sem.wait();
            t_print("after wait: n = {}", .{ctx.n.*});
            ctx.n.* += 1;
            ctx.sem.post();

            t_print("after post: n = {}", .{ctx.n.*});
        }
    };
    const num_threads = 3;
    var sem = Semaphore{ .permits = 1 };
    var threads: [num_threads]std.Thread = undefined;
    var n: i32 = 0;
    var ctx = TestContext{ .sem = &sem, .n = &n };

    for (&threads) |*t| t.* = try std.Thread.spawn(.{}, TestContext.worker, .{&ctx});
    for (threads) |t| t.join();
    sem.wait();
    try testing.expect(n == num_threads);
}

// -----------------------------------------------------------------
// Thread pool
// -----------------------------------------------------------------

const TaskQueue = @import("zliber").thread.TaskQueue;
fn example_tp() !void {
    const allocator = @import("zliber").mem.getFixedBufferAllocator(1024);

    var tq = TaskQueue.init(5, allocator);
    println("thread_pool: ", .{});
    tq.tp.printInfo();

    defer {
        tq.deinit();
    }
    var thread_park = ThreadPark{};
    const TaskContext = struct { thread_park: *ThreadPark, name: []const u8 };
    const runner = struct {
        pub fn f(ctx: *anyopaque) void {
            // _ = ctx;
            var ctx_p: *TaskContext = @ptrCast(@alignCast(ctx));
            for (0..10) |i| {
                sleep(500); // do some work
                const old = num_done.fetchAdd(1, .monotonic);
                t_print("n: {}", .{old});
                ctx_p.thread_park.wakeOne();
                t_print("{s} is running work {}...", .{ ctx_p.name, i });
            }
        }
    }.f;

    // try tq.runTask(task);

    // println("thread_pool after schedule: ", .{});
    // tq.tp.printInfo();
    // while (true) {
    //     const n = num_done.load(.unordered);
    //     if (n == 10) break;
    //     println("Working.. {}/10 done", .{n});
    //     thread_park.wait_timeout(1000);
    //
    //     tq.tp.printInfo();
    // }

    var contexts = [4]TaskContext{
        .{ .thread_park = &thread_park, .name = "task1" },
        .{ .thread_park = &thread_park, .name = "task2" },
        .{ .thread_park = &thread_park, .name = "task3" },
        .{ .thread_park = &thread_park, .name = "task4" },
    };

    var c5 = TaskContext{ .thread_park = &thread_park, .name = "task5" };

    const tasks = [_]TaskQueue.Task{
        .{ .runner = runner, .context = &contexts[0] },
        .{ .runner = runner, .context = &contexts[1] },
        .{ .runner = runner, .context = &contexts[2] },
        .{ .runner = runner, .context = &contexts[3] },
        .{ .runner = runner, .context = &c5 },
    };
    try tq.runTasks(&tasks);
    while (true) {
        const n = num_done.load(.unordered);
        println("Working.. {}/{} done", .{ n, 10 * tasks.len });
        thread_park.wait_timeout(1000);
        // const task_count = tq.consumeCompletions(null);
        // if (task_count > 0) {
        //     println("{} task completed!", .{task_count});
        // }

        println("thread_pool: ", .{});
        tq.tp.printInfo();
        if (tq.tp.isIdle()) {
            tq.tp.shutdown();
            break;
        }
    }

    var completions: [tasks.len]*anyopaque = undefined;
    const completion_count = tq.consumeCompletions(&completions);
    for (completions) |c| {
        const ctx: *TaskContext = @ptrCast(@alignCast(c));
        println("task completed name: {s}", .{ctx.name});
    }

    println("{} Done!", .{completion_count});
}
