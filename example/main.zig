const std = @import("std");
const z = @import("zliber");
const log = z.log;
pub const std_options = z.options;

const P = z.parser.MakeParser(u8);
const TUtils = z.comptime_parser.TUtils;
pub fn main() !void {
    log.set_scopes(.{"parser"});
    const allocator = z.mem.getFixedBufferAllocator(40);

    const parser = comptime P.literal("0x").product(P.oneOf("0123456789").manyOne()).map(i32, struct {
        fn f(ss: struct { z.t.string, z.t.string }) ?i32 {
            var s: z.t.string = undefined;
            return blk: {
                s = concat_str(
                    ss[0],
                    ss[1],
                ) catch |e| break :blk e;
                break :blk std.fmt.parseInt(i32, s, 0);
            } catch |e| {
                log.warn("err => {s}, input => {s}", .{ @errorName(e), s });
                return null;
            };
        }
    }.f);
    const input = "0x1234567abcdefg";
    for (0..5) |_| {
        const res = try parser.parse(allocator, input);
        defer {
            parser.freeResult(res, allocator);
        }
        if (res) |r| {
            log.fprintLn("parse result: {any}\nremains: {s}", .{ r.value, r.slice });
        } else {
            log.warn("parse failed!", .{});
        }
    }
}

fn str_to_int(s: []const u8) i32 {
    return @intCast(s.len);
}

fn find_and_fill(comptime T: type, buf: []?T, v: T) void {
    for (buf) |*b_ptr| {
        if (b_ptr.* == null) {
            b_ptr.* = v;
            break;
        }
    }
}

fn makeBuffer(allocator: std.mem.Allocator) ![]?u8 {
    const buf = try allocator.alloc(?u8, 10);
    for (buf) |*b| {
        b.* = null;
    }

    return buf;
}

fn freeBuffer(buf: []const u8, allocator: std.mem.Allocator) void {
    allocator.free(buf);
}

inline fn concat_str(s1: z.t.string, s2: z.t.string) !z.t.string {
    var arrList = try std.BoundedArray(u8, 100).fromSlice(s1);

    try arrList.appendSlice(s2);
    return arrList.slice();
}

fn paren(comptime R: type, comptime cp: P.Parser(R)) P.Parser(R) {
    return P.item('(').right(cp).left(P.item(')'));
}
