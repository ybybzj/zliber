const std = @import("std");
const zliber = @import("zliber");
const system = std.os.system;
const assert = std.debug.assert;
fn println(comptime fmt: []const u8, args: anytype) void {
    std.debug.print(fmt ++ "\n", args);
}

const sig_handler = system.Sigaction.handler_fn;

extern "c" fn signal(c_int, sig_handler) sig_handler;

extern "c" fn printf(format: [*:0]const u8, ...) c_int;

extern "c" fn strsignal(sig: c_int) [*:0]const u8;

fn sleep(ms: u64) void {
    std.time.sleep(ms * std.time.ns_per_ms);
}

pub fn main() !void {
    const oldHandler = signal(system.SIG.INT, sigHandler);
    if (oldHandler == system.SIG.ERR) {
        switch (system.getErrno(-1)) {
            else => |e| {
                try std.io.getStdErr().writer().print("handle sig INT failed: {s}\n", .{@tagName(e)});
            },
        }
        return error.InvalidSigHandler;
    }

    if (signal(system.SIG.QUIT, sigHandler) == system.SIG.ERR) {
        switch (system.getErrno(-1)) {
            else => |e| {
                try std.io.getStdErr().writer().print("handle sig QUIT failed: {s}\n", .{@tagName(e)});
            },
        }
        return error.InvalidSigHandler;
    }

    for (0..10) |i| {
        println("loop {}", .{i});
        sleep(1000);
    }

    println("loop finished!", .{});

    _ = signal(system.SIG.INT, oldHandler);
}

fn sigHandler(sig: c_int) callconv(.C) void {
    const counter = struct {
        pub var count: c_int = 0;
    };
    if (sig == system.SIG.INT) {
        counter.count += 1;
        _ = printf("Sig %s received %d time!!!\n", strsignal(sig), counter.count);
    } else {
        println("Terminal sig {s} is received!!!", .{strsignal(sig)});
        std.os.exit(0);
    }
}
