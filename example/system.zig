const std = @import("std");
const builtin = @import("builtin");
fn println(comptime fmt: []const u8, args: anytype) void {
    std.debug.print(fmt ++ "\n", args);
}
pub usingnamespace if (@import("root") == @This()) struct {
    pub fn main() !void {
        println("cpu.arch => {}", .{builtin.cpu.arch});
        const n: u8 = 3;
        try testDeferErr(n);
    }
} else struct {};

fn testDeferErr(n: u8) !void {
    if (n == 1) return error.FirstError;
    defer {
        println("[defer]: n => {d}", .{n});
    }

    errdefer |e| {
        println("[errdefer]: n => {d}; err => {s}", .{ n, @errorName(e) });
    }

    if (n == 2) return error.SecondError;
}
