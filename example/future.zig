const std = @import("std");
const zliber = @import("zliber");

const assert = std.debug.assert;
fn println(comptime fmt: []const u8, args: anytype) void {
    std.debug.print(fmt ++ "\n", args);
}

const IO = zliber.io.IO;

pub fn main() !void {
    try async_read_line();
}

fn sync_read_line() !void {
    const input_reader = std.io.getStdIn().reader();
    var buf: [256]u8 = undefined;
    const input = try input_reader.readUntilDelimiter(&buf, '\n');

    println("read line: {s}", .{input});
}

fn async_read_line() !void {
    const ReadBuf = struct {
        buf: []u8,
        completion: IO.Completion = undefined,
        fd_in: std.os.fd_t,
        fd_out: std.os.fd_t,
        io: *IO,
        err: ?anyerror = null,
        const Self = @This();
        pub fn init(io: *IO, buf: []u8, in: std.os.fd_t, out: std.os.fd_t) Self {
            return .{ .io = io, .buf = buf, .fd_in = in, .fd_out = out };
        }

        pub fn read(self: *Self) void {
            self.io.read(*Self, self, onRead, &self.completion, self.fd_in, self.buf, 0);
        }
        pub fn onRead(self: *Self, completion: *IO.Completion, result: IO.ReadError!usize) void {
            _ = completion;
            const n = result catch |e| {
                self.err = e;
                println("read err: {}", .{e});
                return;
            };
            if (n > 0) {
                self.io.write(*Self, self, onWrite, &self.completion, self.fd_out, self.buf, 0);
            }
        }
        pub fn onWrite(self: *Self, completion: *IO.Completion, result: IO.WriteError!usize) void {
            const n = result catch |e| {
                self.err = e;
                return;
            };
            _ = completion;
            println("print {} chars", .{n});
        }
    };

    var io = try IO.init(32, 0);
    defer io.deinit();

    var buf: [256]u8 = undefined;
    var reader = ReadBuf.init(&io, &buf, std.io.getStdIn().handle, std.io.getStdOut().handle);
    reader.read();
    try io.waitAll();
    return reader.err orelse {};
}
