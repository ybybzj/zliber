const std = @import("std");
const assert = std.debug.assert;
fn println(comptime fmt: []const u8, args: anytype) void {
    std.debug.print(fmt ++ "\n", args);
}

pub extern "c" fn printf(format: [*:0]const u8, ...) c_int;
pub fn main() void {
    _ = printf("hello c!!!\n");
}
