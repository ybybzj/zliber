const std = @import("std");
const os = std.os;
const builtin = @import("builtin");
const zlib = @import("zliber");
const net = std.net;
const assert = std.debug.assert;

fn println(comptime fmt: []const u8, args: anytype) void {
    std.debug.print(fmt ++ "\n", args);
}

pub usingnamespace if (@import("root") == @This()) struct {
    pub fn main() !void {
        const page_allocator = std.heap.page_allocator;
        var arena = std.heap.ArenaAllocator.init(page_allocator);
        defer arena.deinit();

        const allocator = arena.allocator();

        const addressList = try net.getAddressList(allocator, "127.0.0.1", 8080);
        defer addressList.deinit();

        if (addressList.addrs.len <= 0) {
            return error.GetAddressFailed;
        }
        for (addressList.addrs) |addr| {
            println("{}", .{addr});
        }

        var io = try zlib.io.IO.init(32, 0);
        defer io.deinit();

        const addr = addressList.addrs[0];
        println("family: {}", .{getFamilyFromAddress(addr)});
        const input = std.io.getStdIn().reader();

        const stdout = std.io.getStdOut().writer();
        var client = io_client(io, input, stdout);
        try client.request(addr);
        try client.wait();
    }
} else struct {};

fn getFamilyFromAddress(address: net.Address) os.sa_family_t {
    const sockaddr: os.sockaddr = address.any;
    return sockaddr.family;
}
const Completion = zlib.io.IO.Completion;
const IO = zlib.io.IO;
fn IoClient(comptime ReaderType: type, comptime WriterType: type) type {
    const FIFO = zlib.std_lib.fifo.LinearFifo(u8, .{ .Static = 4096 });
    return struct {
        io: zlib.io.IO,
        handle: os.socket_t = zlib.io.IO.INVALID_SOCKET,
        input: ReaderType,
        output: WriterType,
        io_completion: zlib.io.IO.Completion = undefined,
        fifo_send: FIFO = FIFO.init(),
        fifo_recv: FIFO = FIFO.init(),
        err: ?anyerror = null,
        is_connected: bool = false,
        const Self = @This();
        pub fn request(self: *Self, address: std.net.Address) !void {
            if (self.handle == zlib.io.IO.INVALID_SOCKET or self.is_connected == false) {
                self.handle = try self.io.open_socket(getFamilyFromAddress(address), os.SOCK.STREAM, os.IPPROTO.TCP);
                self.io.connect(*Self, self, on_connect, &self.io_completion, self.handle, address);
            } else {
                self.do_send();
            }
        }

        pub fn wait(self: *Self) !void {
            try self.io.waitAll();
            if (self.err) |err| return err;
        }
        fn on_connect(self: *Self, completion: *Completion, result: IO.ConnectError!void) void {
            _ = result catch |e| {
                self.err = e;
                return;
            };
            assert(&self.io_completion == completion);
            assert(self.handle != IO.INVALID_SOCKET);
            self.is_connected = true;
            self.do_send();
            // self.do_recv();
        }
        fn do_send(self: *Self) void {
            const fifo = &self.fifo_send;
            const readableLength = fifo.readableLength();

            // println("input len: {d}", .{readableLength});
            if (readableLength > 0) { // send from fifo buffer
                const slice = fifo.readableSlice(0);
                self.io.send(*Self, self, on_send, &self.io_completion, self.handle, slice);
            } else {
                const slice = fifo.writableSlice(0);
                assert(slice.len > 0);

                const r_n = self.input.read(slice) catch |e| {
                    self.err = e;
                    return;
                };
                if (r_n > 0) {
                    fifo.update(r_n);
                    self.do_send();
                }
            }
        }

        fn on_send(self: *Self, completion: *Completion, result: IO.SendError!usize) void {
            _ = completion;
            const r_n = result catch |e| {
                self.err = e;
                return;
            };
            self.fifo_send.discard(r_n);
            if (self.fifo_send.readableLength() > 0) {
                self.do_send();
            } else {
                self.do_recv();
            }
        }

        fn do_recv(self: *Self) void {
            self.flush() catch |e| {
                self.err = e;
                return;
            };
            const slice = self.fifo_recv.writableSlice(0);
            assert(slice.len > 0);
            self.io.recv(*Self, self, on_recv, &self.io_completion, self.handle, slice);
        }

        fn flush(self: *Self) !void {
            while (self.fifo_recv.readableLength() > 0) {
                const slice = self.fifo_recv.readableSlice(0);
                const w_n = try self.output.write(slice);
                self.fifo_recv.discard(w_n);
            }
        }

        fn on_recv(self: *Self, completion: *Completion, result: IO.RecvError!usize) void {
            _ = completion;
            const r_n = result catch |e| {
                self.err = e;
                return;
            };
            if (r_n > 0) {
                self.fifo_recv.update(r_n);

                // println("received {d} from server", .{r_n});

                self.do_recv();

                self.do_send();
            } else {
                self.flush() catch |e| {
                    self.err = e;
                    return;
                };
                self.do_close();
                println("receive from server completed", .{});
            }
        }

        pub fn do_close(self: *Self) void {
            self.io.close(*Self, self, on_close, &self.io_completion, self.handle);
        }
        fn on_close(self: *Self, completion: *Completion, result: IO.CloseError!void) void {
            _ = completion;
            _ = result catch |e| {
                self.err = e;
                return;
            };
            self.is_connected = false;
        }
    };
}

fn io_client(io: IO, input: anytype, output: anytype) IoClient(@TypeOf(input), @TypeOf(output)) {
    return .{ .io = io, .input = input, .output = output };
}
