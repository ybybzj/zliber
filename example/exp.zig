const std = @import("std");
const mem = @import("zliber").mem;
const LinearFifo = std.fifo.LinearFifo;
const http = std.http;
const process = std.process;

pub usingnamespace if (@import("root") == @This()) struct {
    pub fn main() !void {
        var gpa = std.heap.GeneralPurposeAllocator(.{}){};
        defer {
            _ = gpa.deinit();
        }
        const allocator = gpa.allocator();

        const stdout = std.io.getStdOut().writer();

        const uri = try std.Uri.parse("https://www.github.com");
        // var uri = try std.Uri.parse("https://github.com/ziglibs/known-folders/archive/fa75e1bc672952efa0cf06160bbd942b47f6d59b.tar.gz");

        var fifo = LinearFifo(u8, .{ .Static = 1024 }).init();
        defer {
            fifo.deinit();
        }
        //
        var http_client = http.Client{
            .allocator = allocator,
        };
        defer {
            http_client.deinit();
        }

        var arena = std.heap.ArenaAllocator.init(http_client.allocator);

        try http_client.initDefaultProxies(arena.allocator());
        // proxy do not support tunnel connection
        if (http_client.https_proxy) |https_proxy| {
            https_proxy.supports_connect = false;
        }

        //

        try stdout.print("start requesting {+/#} ...\n", .{uri});

        var arrList = std.ArrayList(u8).init(allocator);
        defer {
            arrList.deinit();
        }

        const fetchOpts: http.Client.FetchOptions = .{
            .response_storage = .{ .dynamic = &arrList },
            .location = .{
                .uri = uri,
            },
            .method = http.Method.GET,
        };
        _ = try http_client.fetch(fetchOpts);

        const body = try arrList.toOwnedSlice();
        defer {
            allocator.free(body);
        }

        var resultBuffer = std.io.fixedBufferStream(body);

        try fifo.pump(resultBuffer.reader(), stdout);

        try stdout.print("\n", .{});
    }
} else struct {};
