const std = @import("std");
const os = std.os;
const builtin = @import("builtin");
const zlib = @import("zliber");
const net = std.net;
const IO = zlib.io.IO;
const assert = std.debug.assert;

fn println(comptime fmt: []const u8, args: anytype) void {
    std.debug.print(fmt ++ "\n", args);
}
pub usingnamespace if (@import("root") == @This()) struct {
    pub fn main() !void {
        println("[{s}]Running {s}!", .{ builtin.zig_version_string, @src().file });

        // const _time = Time{};
        println("Local time is: {s}!", .{zlib.date.Date.now()});

        const allocator = std.heap.page_allocator;
        const address = try zlib.net.getServAddress(allocator, .{ .port = 8080, .hints = .{ .family = .ip4 } });
        println("\nserver address is: {}", .{address});

        println("Creating server...", .{});
        var io = try IO.init(32, 0);
        defer io.deinit();
        var server = StreamServer.init(.{}, &io);
        defer {
            println("Shutdown server...", .{});
            server.deinit();
        }

        println("Listening...", .{});

        try server.listen(address);
        //
        println("Waiting for connection...", .{});

        assert(server.sockfd != null);

        server.accept();

        try io.waitAll();

        server.close();
        if (server.err) |e| return e;

        // const con = try server.accept();
        // println("Client [{}] is connected...", .{con.address});
        //
        // println("Reading request...", .{});
        // var buf: [1024]u8 = undefined;
        // // while (true) {
        // const bytes_received = try con.stream.read(&buf);
        //
        // const slice = buf[0..bytes_received];
        // // if (std.mem.eql(u8, "done", slice)) {
        // //     break;
        // // }
        // println("Received {d} bytes.", .{bytes_received});
        //
        // println("Sending response...", .{});
        // const resp = con.stream.writer();
        // try resp.print("HTTP/1.1 200 OK\r\n" ++
        //     "Connection: close\r\n" ++
        //     "Content-Type: text/plain\r\n\r\n" ++
        //     "Local time is: {s}, and received content is: {s}\r\n", .{ zlib.date.Date.now(), slice });
        // // }
        // println("Finished.", .{});
    }
} else struct {};

const FIFO = std.fifo.LinearFifo(u8, .{ .Static = 4096 });
const StreamServer = struct {
    err: ?anyerror = null,
    fifo: FIFO = FIFO.init(),
    io: *IO,
    completion: IO.Completion = undefined,
    client_fd: os.socket_t = IO.INVALID_SOCKET,
    /// Copied from `Options` on `init`.
    kernel_backlog: u31,
    reuse_address: bool,
    reuse_port: bool,

    /// `undefined` until `listen` returns successfully.
    listen_address: std.net.Address,

    sockfd: ?os.socket_t,

    pub const Options = struct {
        /// How many connections the kernel will accept on the application's behalf.
        /// If more than this many connections pool in the kernel, clients will start
        /// seeing "Connection refused".
        kernel_backlog: u31 = 128,

        /// Enable SO.REUSEADDR on the socket.
        reuse_address: bool = true,

        /// Enable SO.REUSEPORT on the socket.
        reuse_port: bool = true,
    };

    /// After this call succeeds, resources have been acquired and must
    /// be released with `deinit`.
    pub fn init(options: Options, io: *IO) StreamServer {
        return StreamServer{
            .io = io,
            .sockfd = null,
            .kernel_backlog = options.kernel_backlog,
            .reuse_address = options.reuse_address,
            .reuse_port = options.reuse_port,
            .listen_address = undefined,
        };
    }

    /// Release all resources. The `StreamServer` memory becomes `undefined`.
    pub fn deinit(self: *StreamServer) void {
        self.close();
        self.* = undefined;
    }

    pub fn listen(self: *StreamServer, address: std.net.Address) !void {
        const sock_flags = os.SOCK.STREAM | os.SOCK.CLOEXEC | os.SOCK.NONBLOCK;
        const proto = if (address.any.family == os.AF.UNIX) @as(u32, 0) else os.IPPROTO.TCP;

        const sockfd = try os.socket(address.any.family, sock_flags, proto);
        self.sockfd = sockfd;
        errdefer {
            os.close(sockfd);
            self.sockfd = null;
        }

        if (self.reuse_address) {
            try os.setsockopt(
                sockfd,
                os.SOL.SOCKET,
                os.SO.REUSEADDR,
                &std.mem.toBytes(@as(c_int, 1)),
            );
        }
        if (@hasDecl(os.SO, "REUSEPORT") and self.reuse_port) {
            try os.setsockopt(
                sockfd,
                os.SOL.SOCKET,
                os.SO.REUSEPORT,
                &std.mem.toBytes(@as(c_int, 1)),
            );
        }

        var socklen = address.getOsSockLen();
        try os.bind(sockfd, &address.any, socklen);
        try os.listen(sockfd, self.kernel_backlog);
        try os.getsockname(sockfd, &self.listen_address.any, &socklen);
    }

    pub fn accept(ctx: *StreamServer) void {
        ctx.io.accept(*StreamServer, ctx, StreamServer.on_accept, &ctx.completion, ctx.sockfd.?);
    }

    fn on_accept(ctx: *StreamServer, completion: *IO.Completion, result: IO.AcceptError!os.socket_t) void {
        _ = completion;
        const fd = result catch |err| {
            ctx.err = err;
            return;
        };
        ctx.client_fd = fd;
        var accepted_addr: std.net.Address = undefined;
        var adr_len: os.socklen_t = @sizeOf(std.net.Address);

        os.getpeername(fd, &accepted_addr.any, &adr_len) catch |e| {
            ctx.err = e;
            return;
        };
        println("Client [{}] is connected...", .{accepted_addr});

        do_recv(ctx);
    }

    fn do_recv(ctx: *StreamServer) void {
        if (ctx.fifo.writableLength() > 0) {
            println("do_recv", .{});
            ctx.io.recv(*StreamServer, ctx, on_recv, &ctx.completion, ctx.client_fd, ctx.fifo.writableSlice(0));
        } else {
            do_send(ctx);
        }
    }

    fn on_recv(ctx: *StreamServer, completion: *IO.Completion, result: IO.RecvError!usize) void {
        _ = completion;
        const r_n = result catch |e| {
            ctx.err = e;
            return;
        };
        println("client[{}] received {d} bytes...", .{ ctx.client_fd, r_n });
        if (r_n > 0) {
            ctx.fifo.update(r_n);
            do_send(ctx);
        }
    }

    fn do_send(ctx: *StreamServer) void {
        if (ctx.fifo.readableLength() > 0) {
            ctx.io.send(*StreamServer, ctx, on_send, &ctx.completion, ctx.client_fd, ctx.fifo.readableSlice(0));
        } else {
            do_recv(ctx);
        }
    }

    fn on_send(ctx: *StreamServer, completion: *IO.Completion, result: IO.SendError!usize) void {
        const s_n = result catch |e| {
            ctx.err = e;
            return;
        };
        _ = completion;
        if (s_n > 0) {
            println("client[{}] send {d} bytes...", .{ ctx.client_fd, s_n });

            ctx.fifo.discard(s_n);
            do_recv(ctx);
        }
    }

    pub fn close(self: *StreamServer) void {
        // if (self.client_fd != IO.INVALID_SOCKET) {
        //     os.close(self.client_fd);
        // }
        if (self.sockfd) |fd| {
            os.close(fd);
            self.sockfd = null;
            self.listen_address = undefined;
        }
    }
};
