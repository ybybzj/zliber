const std = @import("std");
const assert = std.debug.assert;
const testing = std.testing;
const zlib = @import("zliber");
const ThreadPool = zlib.thread.ThreadPool;
var val: i32 = 0;
test "thread pool" {
    var tp = ThreadPool.init(.{ .max_threads = 2 });
    defer tp.deinit();

    var s1 = TaskCallback{ .index = 0 };
    var s2 = TaskCallback{ .index = 1 };
    var s3 = TaskCallback{ .index = 2 };
    var s4 = TaskCallback{ .index = 3 };
    var batch = ThreadPool.Batch{};

    batch.add(&s1.task);
    batch.add(&s2.task);
    batch.add(&s3.task);
    batch.add(&s4.task);

    tp.schedule(batch);
    // std.time.sleep(1 * std.time.ns_per_s);

    TaskCallback.x.store(1, .unordered);
    val = 23;
}
const Atomic = std.atomic.Value;
const ThreadCallback = *const fn (*ThreadPool.Task, *ThreadPool) void;
const TaskCallback = struct {
    task: ThreadPool.Task = ThreadPool.Task{ .callback = cb },
    index: u8,
    const Self = @This();

    var t_num = Atomic(u16).init(0);
    pub var x: Atomic(i32) = Atomic(i32).init(0);
    pub fn cb(t: *ThreadPool.Task, tp: *ThreadPool) void {
        _ = t;
        // const self = @fieldParentPtr(Self, "task", t);
        assert(val == 23);

        while (x.load(.unordered) == 0) {
            std.time.sleep(2 * std.time.ns_per_s);
        }

        _ = t_num.fetchAdd(1, .monotonic);
        // std.time.sleep(3 * std.time.ns_per_s);
        if (t_num.load(.unordered) >= 4) {
            // const _x = x.load(.monotonic);
            tp.shutdown();
        }
    }
};

const ThreadPark = zlib.thread.ThreadPark;

fn sleep(ms: u64) void {
    std.time.sleep(ms * std.time.ns_per_ms);
}

var num_done: Atomic(usize) = Atomic(usize).init(0);
test "task queue" {
    const TaskQueue = zlib.thread.TaskQueue;
    const allocator = zlib.mem.getFixedBufferAllocator(1024);

    var tq = TaskQueue.init(5, allocator);

    defer {
        tq.deinit();
    }
    var thread_park = ThreadPark{};
    const TaskContext = struct { thread_park: *ThreadPark, name: []const u8 };
    const runner = struct {
        pub fn f(ctx: *anyopaque) void {
            // _ = ctx;
            var ctx_p: *TaskContext = @ptrCast(@alignCast(ctx));
            for (0..10) |_| {
                sleep(10); // do some work
                _ = num_done.fetchAdd(1, .monotonic);
                ctx_p.thread_park.wakeOne();
            }
        }
    }.f;

    var contexts = [4]TaskContext{
        .{ .thread_park = &thread_park, .name = "task1" },
        .{ .thread_park = &thread_park, .name = "task2" },
        .{ .thread_park = &thread_park, .name = "task3" },
        .{ .thread_park = &thread_park, .name = "task4" },
    };

    var c5 = TaskContext{ .thread_park = &thread_park, .name = "task5" };

    const tasks = [_]TaskQueue.Task{
        .{ .runner = runner, .context = &contexts[0] },
        .{ .runner = runner, .context = &contexts[1] },
        .{ .runner = runner, .context = &contexts[2] },
        .{ .runner = runner, .context = &contexts[3] },
        .{ .runner = runner, .context = &c5 },
    };
    try tq.runTasks(&tasks);
    while (true) {
        thread_park.wait_timeout(1000);

        if (tq.isIdle()) {
            defer tq.shutdown();

            const n = num_done.load(.unordered);
            try testing.expectEqual(50, n);

            break;
        }
    }

    const consumeCount = tq.consumeCompletions(null);

    try testing.expectEqual(tasks.len, consumeCount);
}
