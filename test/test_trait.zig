const zliber = @import("zliber");
const std = @import("std");
const testing = std.testing;
test "trait" {
    const trait = zliber.trait;
    const Kind = enum(u1) { left, right };
    const S =
        struct {
        a: Kind,
        b: i32,
        pub fn print() void {}
        pub const flag = "flag";
    };
    // const ITest = trait.ContainerTrait(&.{ .{ .field, "a", isType(u32) }, .{ .field, "b", isType(i32) }, .{ .decl, "print", trait.is(.Fn) }, .{ .decl, "flag", trait.is(.Array) } });
    const ITest = trait.TraitFromStruct(S);
    const TestS = trait.implTrait(ITest, struct {
        a: Kind,
        b: i32,
        pub fn print() void {}
        pub const flag = "";
    });
    try testing.expect(trait.isFieldOf("a", .Enum)(TestS));
    try testing.expect(trait.isFieldOf("b", .Int)(TestS));
    try testing.expect(trait.isDeclOf("print", .Fn)(TestS));
    try testing.expect(trait.isDeclOf("flag", .Pointer)(TestS));
}
