test {
    _ = @import("test_parser.zig");
    _ = @import("test_io.zig");
    _ = @import("test_date.zig");
    _ = @import("test_trait.zig");
    _ = @import("test_thread_task.zig");
}
