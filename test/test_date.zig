const std = @import("std");
const zliber = @import("zliber");

const testing = std.testing;
const Date = zliber.date.Date;
test "date" {
    const _now = std.time.timestamp();
    const now = @as(u64, @intCast(_now));
    const date = Date.fromEpochSeconds(now);
    try testing.expectEqual(now, date.toEpochSeconds());
}
