const std = @import("std");
const testing = std.testing;
const P = @import("zliber").parser.MakeParser(u8);
const Allocator = std.mem.Allocator;
fn stringEql(s1: []const u8, s2: []const u8) bool {
    return std.mem.eql(u8, s1, s2);
}

test "literal" {
    const p = P.literal("abc");
    const allocator = testing.allocator;
    const res = try p.parse(allocator, "abcde");
    try testing.expectEqualStrings("abc", res.?.value);
    try testing.expectEqualStrings("de", res.?.slice);
}

test "map combinator" {
    const p = P.literal("abc").map(usize, struct {
        fn f(s: []const u8) ?usize {
            return s.len;
        }
    }.f);
    const allocator = testing.allocator;
    const res = try p.parse(allocator, "abcde");
    try testing.expect(res != null);
    try testing.expectEqual(@as(usize, 3), res.?.value);
    try testing.expect(stringEql("de", res.?.slice));
}

test "mapAlloc combinator" {
    const allocator = testing.allocator;

    const p = blk: {
        const p1 = P.literal("jack");
        const mapFn = struct {
            fn f(name: []const u8, _allocator: Allocator) !?[]u8 {
                const hello_len = "hello".len;
                const len = hello_len + 1 + name.len;
                const buf = try _allocator.alloc(u8, len);
                @memset(buf, ' ');
                @memcpy(buf[0..hello_len], "hello"[0..]);
                @memcpy(buf[hello_len + 1 ..], name);
                return buf[0..len];
            }
        }.f;

        const freeFn = struct {
            fn f(buf: []const u8, _allocator: Allocator) void {
                _allocator.free(buf);
            }
        }.f;

        break :blk p1.mapAlloc([]u8, mapFn, freeFn);
    };
    const res = try p.parse(allocator, "jackaaa");
    defer {
        p.freeResult(res, allocator);
    }

    try testing.expect(res != null);
    try testing.expect(stringEql("hello jack", res.?.value));
    try testing.expect(stringEql("aaa", res.?.slice));
}

test "item parser" {
    const allocator = testing.allocator;
    const p = P.item('c');

    const r1 = try p.parse(allocator, "cba");
    try testing.expect(r1 != null);
    try testing.expect('c' == r1.?.value);
    try testing.expect(stringEql("ba", r1.?.slice));

    const r2 = try p.parse(allocator, "baa");
    try testing.expect(r2 == null);
}

test "oneOf parser" {
    const allocator = testing.allocator;

    const p = P.oneOf("abc");

    const r1 = try p.parse(allocator, "abc");

    try testing.expect(r1 != null);
    try testing.expect('a' == r1.?.value);
    try testing.expect(stringEql("bc", r1.?.slice));

    const r2 = try p.parse(allocator, "bac");
    try testing.expect(r2 != null);
    try testing.expect('b' == r2.?.value);
    try testing.expect(stringEql("ac", r2.?.slice));

    const r3 = try p.parse(allocator, "efd");
    try testing.expect(r3 == null);
}

test "product combinator" {
    const p = P.literal("abc").product(P.literal("efg"));
    const allocator = testing.allocator;
    const res = try p.parse(allocator, "abcefghj");

    defer {
        p.freeResult(res, allocator);
    }

    try testing.expect(res != null);
    try testing.expect(stringEql("abc", res.?.value[0]));
    try testing.expect(stringEql("efg", res.?.value[1]));
    try testing.expect(stringEql("hj", res.?.slice));
}

test "many(One) combinator" {
    const many_p = P.oneOf("0123456789").many();
    const allocator = testing.allocator;
    const r1 = try many_p.parse(allocator, "1234asd");
    defer {
        many_p.freeResult(r1, allocator);
    }
    try testing.expect(r1 != null);
    try testing.expect(stringEql("1234", r1.?.value));
    try testing.expect(stringEql("asd", r1.?.slice));

    const r2 = try many_p.parse(allocator, "asd123");
    defer {
        many_p.freeResult(r2, allocator);
    }

    try testing.expect(r2 != null);
    try testing.expect(r2.?.value.len == 0);

    const manyOne_p = P.oneOf("0123456789").manyOne();

    const r3 = try many_p.parse(allocator, "1234asd");
    defer {
        manyOne_p.freeResult(r3, allocator);
    }
    try testing.expect(r3 != null);
    try testing.expect(stringEql("1234", r3.?.value));
    try testing.expect(stringEql("asd", r3.?.slice));

    const r4 = try manyOne_p.parse(allocator, "asd123");
    try testing.expect(r4 == null);
}

test "left combinator" {
    const p = P.oneOf("0123456789").manyOne().left(P.oneOf("abc").many());

    const allocator = testing.allocator;
    const r1 = try p.parse(allocator, "123abcd");
    defer {
        p.freeResult(r1, allocator);
    }

    try testing.expect(r1 != null);
    try testing.expect(stringEql("123", r1.?.value));
    try testing.expectEqualStrings("d", r1.?.slice);

    const r2 = try p.parse(allocator, "123db");
    defer {
        p.freeResult(r2, allocator);
    }

    try testing.expect(r2 != null);
    try testing.expect(r2.?.value.len == 3);
    try testing.expect(stringEql("db", r2.?.slice));
}

test "right combinator" {
    const p = P.oneOf("0123456789").manyOne().right(P.oneOf("abc").manyOne());

    const allocator = testing.allocator;
    const r1 = try p.parse(allocator, "123abcd");
    defer {
        p.freeResult(r1, allocator);
    }

    try testing.expect(r1 != null);
    try testing.expect(stringEql("abc", r1.?.value));
    try testing.expect(stringEql("d", r1.?.slice));

    const r2 = try p.parse(allocator, "123db");
    defer {
        p.freeResult(r2, allocator);
    }

    try testing.expect(r2 == null);
}

test "either combinator" {
    const allocator = testing.allocator;
    const p = P.literal("abc").either(P.oneOf("def").manyOne());

    const r1 = try p.parse(allocator, "abced");
    defer {
        p.freeResult(r1, allocator);
    }
    try testing.expect(r1 != null);
    try testing.expect(stringEql("abc", r1.?.value));
    try testing.expect(stringEql("ed", r1.?.slice));

    const r2 = try p.parse(allocator, "defoo");
    defer {
        p.freeResult(r2, allocator);
    }

    try testing.expect(r2 != null);
    try testing.expect(stringEql("def", r2.?.value));
    try testing.expect(stringEql("oo", r2.?.slice));

    const r3 = try p.parse(allocator, "aacc");
    defer {
        p.freeResult(r3, allocator);
    }

    try testing.expect((r3 == null));
}

test "fold combinator without alloc" {
    const n_p = P.oneOf("0123456789").manyOne().map(i32, struct {
        fn f(s: []const u8) ?i32 {
            return std.fmt.parseInt(i32, s, 0) catch |e| {
                std.debug.print("errer happend => {s}\n", .{@errorName(e)});
                return null;
            };
        }
    }.f);
    const nn_p = P.item(',').right(n_p);

    const p = nn_p.fold(n_p, struct {
        fn f(r: i32, i: i32) !i32 {
            return r + i;
        }
    }.f);
    const allocator = testing.allocator;

    const res = try p.parse(allocator, "1,2,3");

    try testing.expect(res != null);
    try testing.expectEqual(@as(i32, 6), res.?.value);
}

fn Collector(comptime T: type) type {
    return struct {
        pub const Collection = std.ArrayList(T);
        pub fn init(allocator: Allocator) !*Collection {
            const list = try allocator.create(Collection);

            list.* = Collection.init(allocator);
            return list;
        }

        pub fn deinit(list: *Collection, allocator: Allocator) void {
            list.*.deinit();
            allocator.destroy(list);
        }
    };
}

fn letterCount(s: []const u8) ?usize {
    return s.len;
}

test "fold with alloc" {
    const p = blk: {
        const p_word = P.oneOf("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-").manyOne();
        const p_sep = P.oneOf(", |;").many();
        const p_lc_w = p_word.map(usize, letterCount);

        const p_patten = p_lc_w.left(p_sep);

        const Collection = Collector(usize).Collection;
        const initFn = Collector(usize).init;
        const freeFn = Collector(usize).deinit;
        const p_init = P.ret(*Collection, initFn, freeFn);
        break :blk p_patten.fold(p_init, struct {
            fn fold(list: *Collection, wc: usize) !*Collection {
                try list.append(wc);
                return list;
            }
        }.fold).map(usize, struct {
            fn map(list: *Collection) ?usize {
                var res: usize = 0;
                return for (list.items) |wc| {
                    res += wc;
                } else res;
            }
        }.map);
    };
    const allocator = testing.allocator;
    const res = try p.parse(allocator, "asd,wrwe,sdfs00,,..");
    try testing.expect(res != null);
    try testing.expectEqual(@as(usize, 3 + 4 + 6), res.?.value);
    try testing.expect(stringEql("..", res.?.slice));
}

test "choose parser" {
    const allocator = testing.allocator;

    const p = comptime blk: {
        const p1 = P.literal("abc");
        const p2 = P.oneOf("efg").manyOne();
        break :blk P.choose(&.{ p1, p2 });
    };

    const r1 = p.parse(allocator, "abcde") catch unreachable;
    defer {
        p.freeResult(r1, allocator);
    }

    try testing.expect(r1 != null);
    try testing.expect(stringEql("abc", r1.?.value));
    try testing.expect(stringEql("de", r1.?.slice));

    const r2 = p.parse(allocator, "efgbc") catch unreachable;
    defer {
        p.freeResult(r2, allocator);
    }
    try testing.expect(r2 != null);
    try testing.expect(stringEql("efg", r2.?.value));
    try testing.expect(stringEql("bc", r2.?.slice));

    const r3 = p.parse(allocator, "sdfs") catch unreachable;

    try testing.expect(r3 == null);
}

fn paren(comptime p: P.Parser) P.Parser {
    return P.item('(').right(p).left(P.item(')'));
}
test "self recursion parser combinator" {
    const p = P.rec([]const u8, struct {
        fn f(comptime p: P.Parser) P.Parser {
            return P.oneOf("abcd").manyOne().either(paren(p));
        }
    }.f);
    const allocator = testing.allocator;

    const res = try p.parse(allocator, "((abbd))asda");
    defer {
        p.freeResult(res, allocator);
    }
    try testing.expect(res != null);
    try testing.expectEqualStrings("abbd", res.?.value);
}

fn OpFn(comptime T: type) type {
    return *const fn (T, T) T;
}

fn chain(comptime p: P.Parser, comptime op: P.Parser) P.Parser {
    const PR = p.Result();
    const OP = op.Result();
    std.debug.assert(OP == OpFn(PR));
    const product_p = op.product(p);
    return product_p.fold(p, struct {
        fn f(l: PR, pair: product_p.Result()) !PR {
            const opFn = pair[0];
            return opFn(l, pair[1]);
        }
    }.f);
}

test "simple arithmetic expression" {
    const Op_i32 = OpFn(i32);
    const ops = struct {
        pub fn add(a: i32, b: i32) i32 {
            return a + b;
        }
        pub fn sub(a: i32, b: i32) i32 {
            return a - b;
        }

        pub fn mul(a: i32, b: i32) i32 {
            return a * b;
        }

        pub fn div(a: i32, b: i32) i32 {
            return @divTrunc(a, b);
        }
    };
    const add_p = P.item('+').right(P.retConst(Op_i32, ops.add));
    const sub_p = P.item('-').right(P.retConst(Op_i32, ops.sub));
    const mul_p = P.item('*').right(P.retConst(Op_i32, ops.mul));
    const div_p = P.item('/').right(P.retConst(Op_i32, ops.div));

    const num_p = P.oneOf("0123456789").manyOne().map(i32, struct {
        fn f(s: []const u8) ?i32 {
            return std.fmt.parseInt(i32, s, 0) catch {
                return null;
            };
        }
    }.f);

    const p = P.rec(i32, struct {
        fn f(comptime expr: P.Parser) P.Parser {
            const factor = paren(expr).either(num_p);
            const term = chain(factor, mul_p.either(div_p));
            return chain(term, add_p.either(sub_p));
        }
    }.f);
    const allocator = testing.allocator;
    const res = try p.parse(allocator, "(11+2)*(54-43)abc");
    defer {
        p.freeResult(res, allocator);
    }
    try testing.expect(res != null);
    try testing.expectEqual(@as(i32, 143), res.?.value);
    try testing.expectEqualStrings("abc", res.?.slice);
}
