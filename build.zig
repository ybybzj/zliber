const std = @import("std");
const B = @import("./build/build.zig");
const pkgs = .{.{ .name = "zliber", .source = "src/zliber.zig" }};

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    const run_step = b.step("run", "Run the example");
    const exe_name: ?[]const u8 = b.option([]const u8, "name", "Select execution source file");

    if (exe_name) |e_name| {
        var buf: [100]u8 = undefined;
        const src_path = std.fmt.bufPrint(&buf, "./example/{s}.zig", .{e_name}) catch |e| @panic(@errorName(e));
        const exe_step = B.step.addSimpleExe(b, .{ .name = e_name, .src = src_path, .target = target, .optimize = optimize, .modules = &pkgs });

        run_step.dependOn(exe_step);
    } else {
        const exe_step = B.step.addSimpleExe(b, .{ .name = "main", .src = "./example/main.zig", .target = target, .optimize = optimize, .modules = &pkgs });

        run_step.dependOn(exe_step);
    }

    const test_step = B.step.addTests(b, .{ .src = "test/tests.zig", .target = target, .optimize = optimize, .modules = &pkgs });

    const run_unit_tests = b.step("test", "Run unit tests");
    run_unit_tests.dependOn(test_step);
}
