const std = @import("std");
const utils = @import("utils.zig");
pub const ModuleConfig = utils.ModuleConfig;
const addModule = utils.addModule;
const Build = std.Build;
const Target = std.Build.ResolvedTarget;
const OptimizeMode = std.builtin.Mode;

pub const ExeStepConfig = struct { name: []const u8, src: []const u8, target: Target, optimize: OptimizeMode, modules: []const ModuleConfig = &.{} };

pub fn addSimpleExe(b: *Build, config: ExeStepConfig) *std.Build.Step {
    const exe = b.addExecutable(.{ .name = config.name, .root_source_file = .{ .path = config.src }, .target = config.target, .optimize = config.optimize });

    for (config.modules) |mod| {
        addModule(exe, b, mod);
    }

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    return &run_cmd.step;
}

pub const TestStepConfig = struct { src: []const u8, target: Target, optimize: OptimizeMode, modules: []const ModuleConfig = &.{} };

pub fn addTests(b: *std.Build, config: TestStepConfig) *std.Build.Step {
    const unit_tests = b.addTest(.{
        .root_source_file = .{ .path = config.src },
        .target = config.target,
        .optimize = config.optimize,
    });
    for (config.modules) |mod| {
        addModule(unit_tests, b, mod);
    }
    const run_unit_tests = b.addRunArtifact(unit_tests);
    return &run_unit_tests.step;
}
