var use_color_escapes = false;

pub const ClsCLr = enum {
    cls_Black,
    cls_Red,
    cls_Green,
    cls_Yellow,
    cls_Blue,
    cls_Magenta,
    cls_Cyan,
    cls_White,

    cls_hBlack,
    cls_hRed,
    cls_hGreen,
    cls_hYellow,
    cls_hBlue,
    cls_hMagenta,
    cls_hCyan,
    cls_hWhite,

    //background color
    cls_bBlack,
    cls_bRed,
    cls_bGreen,
    cls_bYellow,
    cls_bBlue,
    cls_bMagenta,
    cls_bCyan,
    cls_bWhite,

    cls_bhBlack,
    cls_bhRed,
    cls_bhGreen,
    cls_bhYellow,
    cls_bhBlue,
    cls_bhMagenta,
    cls_bhCyan,
    cls_bhWhite,

    //font
    cls_Bold, //bold and high intensity
    cls_Underline,

    cls_Crossout,
    cls_Conceal,
};

fn clsclr_str(c: ClsCLr) []const u8 {
    return switch (c) {
        //foreground color
        .cls_Black => "30",
        .cls_Red => "31",
        .cls_Green => "32",
        .cls_Yellow => "33",
        .cls_Blue => "34",
        .cls_Magenta => "35",
        .cls_Cyan => "36",
        .cls_White => "37",

        .cls_hBlack => "90",
        .cls_hRed => "91",
        .cls_hGreen => "92",
        .cls_hYellow => "93",
        .cls_hBlue => "94",
        .cls_hMagenta => "95",
        .cls_hCyan => "96",
        .cls_hWhite => "97",

        //background color
        .cls_bBlack => "40",
        .cls_bRed => "41",
        .cls_bGreen => "42",
        .cls_bYellow => "43",
        .cls_bBlue => "44",
        .cls_bMagenta => "45",
        .cls_bCyan => "46",
        .cls_bWhite => "47",

        .cls_bhBlack => "100",
        .cls_bhRed => "101",
        .cls_bhGreen => "102",
        .cls_bhYellow => "103",
        .cls_bhBlue => "104",
        .cls_bhMagenta => "105",
        .cls_bhCyan => "106",
        .cls_bhWhite => "107",

        //font
        .cls_Bold => "1", //bold and high intensity
        .cls_Underline => "4",

        .cls_Crossout => "9",
        .cls_Conceal => "8",
    };
}
const ansi_color_reset = "\x1b[0m";

pub inline fn cls_style(comptime str: []const u8, comptime styles: []const ClsCLr) []const u8 {
    const style_str = "\x1b[" ++
        (blk: {
        var style_num: []const u8 = "";
        inline for (styles) |s| {
            const ss = clsclr_str(s);
            style_num = style_num ++ if (ss.len > 0) ";" ++ ss else "";
        }
        break :blk style_num;
    }) ++ "m";
    return style_str ++ str ++ ansi_color_reset;
}

pub inline fn clsclr(comptime ss: []const []const u8) []const u8 {
    var str: []const u8 = "";
    inline for (ss) |s| {
        str = str ++ s;
    }

    return str;
}

const std = @import("std");
const builtin = @import("builtin");
pub fn is_clsclr_support() bool {
    if (std.io.getStdErr().supportsAnsiEscapeCodes()) {
        return true;
    } else if (builtin.os.tag == .windows) {
        const w32 = struct {
            const WINAPI = std.os.windows.WINAPI;
            const DWORD = std.os.windows.DWORD;
            const ENABLE_VIRTUAL_TERMINAL_PROCESSING = 0x0004;
            const STD_ERROR_HANDLE: DWORD = @bitCast(@as(i32, -12));
            extern "kernel32" fn GetStdHandle(id: DWORD) callconv(WINAPI) ?*anyopaque;
            extern "kernel32" fn GetConsoleMode(console: ?*anyopaque, out_mode: *DWORD) callconv(WINAPI) u32;
            extern "kernel32" fn SetConsoleMode(console: ?*anyopaque, mode: DWORD) callconv(WINAPI) u32;
        };
        const handle = w32.GetStdHandle(w32.STD_ERROR_HANDLE);
        var mode: w32.DWORD = 0;
        if (w32.GetConsoleMode(handle, &mode) != 0) {
            mode |= w32.ENABLE_VIRTUAL_TERMINAL_PROCESSING;
            return w32.SetConsoleMode(handle, mode) != 0;
        }
    }
    return false;
}
