const std = @import("std");

pub const ModuleConfig = struct { name: []const u8, source: []const u8 };
pub fn addModule(step: *std.Build.Step.Compile, b: *std.Build, config: ModuleConfig) void {
    const module = b.addModule(config.name, .{ .root_source_file = .{ .path = config.source } });
    step.root_module.addImport(config.name, module);
}
